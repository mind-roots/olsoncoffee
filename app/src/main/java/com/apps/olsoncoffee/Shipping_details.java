package com.apps.olsoncoffee;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.apps.olsoncoffee.Database.DatabaseQueries;
import com.apps.olsoncoffee.model.JSONParser;
import com.apps.olsoncoffee.model.Methods;
import com.apps.olsoncoffee.model.ModelCartArray;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalService;

import org.json.JSONObject;

import java.util.ArrayList;

public class Shipping_details extends AppCompatActivity {

    public ArrayList<String> Shipping_sArraylist_state = new ArrayList<>();
    public ArrayList<String> Shipping_sArraylist_country = new ArrayList<>();
    private int mNotificationsCount = 0;

    LinearLayout paynow;
    DatabaseQueries query;


    private EditText et_shipping_email;
    private EditText et_shipping_first_name;
    private EditText et_shipping_last_name;
    private EditText et_shipping_company_name;
    private EditText et_shipping_phone_num;
    private EditText et_shipping_address_1;
    private EditText et_shipping_address_2;
    private EditText et_shipping_city;
    private Spinner spinner_shipping_country;
    private Spinner spinner_shipping_state;
    private EditText et_shipping_zip_code;

    ProgressDialog progess;
    public ArrayList<ModelCartArray> json_arr = new ArrayList<>();

    String json;
    boolean network;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_details);
        init();
        clickEvent();
   //     setdata();

    }


    private void init() {
        progess = new ProgressDialog(Shipping_details.this);
        progess.setIndeterminate(true);
        progess.setMessage("Fetching");
        progess.setCancelable(false);
        network = Methods.isNetworkConnected(getApplicationContext());
        Methods.HideKayboard(Shipping_details.this);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.olson_toolbar);
        setSupportActionBar(mToolbar);

        query = new DatabaseQueries(Shipping_details.this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle(" ");
            mToolbar.setNavigationIcon(R.mipmap.ic_back);

        } catch (Exception e) {
            e.printStackTrace();
        }

        paynow = (LinearLayout) findViewById(R.id.shipment);

        et_shipping_email = (EditText) findViewById(R.id.email_for_shipping);
        et_shipping_first_name = (EditText) findViewById(R.id.first_name_for_shipping);
        et_shipping_last_name = (EditText) findViewById(R.id.last_name_for_shipping);
        et_shipping_company_name = (EditText) findViewById(R.id.company_name_for_shipping);
        et_shipping_phone_num = (EditText) findViewById(R.id.phone_number_for_shipping);
        et_shipping_address_1 = (EditText) findViewById(R.id.address_line_1_for_shipping);
        et_shipping_address_2 = (EditText) findViewById(R.id.address_line_2_for_shipping);
        et_shipping_city = (EditText) findViewById(R.id.city_name_for_shipping);
        spinner_shipping_country = (Spinner) findViewById(R.id.shipping_detail_choose_country_spinner);
        spinner_shipping_state = (Spinner) findViewById(R.id.shipping_detail_choose_state_spinner);
        et_shipping_zip_code = (EditText) findViewById(R.id.zip_code_for_shipping);


        populate_State_spinner(Shipping_sArraylist_state);
        populate_Country_spinner(Shipping_sArraylist_country);


    }

    private void setdata() {
        et_shipping_email.setText(Utils.shipping_email);
        et_shipping_first_name.setText(Utils.shipping_first_name);
        et_shipping_last_name.setText(Utils.shipping_last_name);
        et_shipping_company_name.setText(Utils.shipping_company_name);
        et_shipping_phone_num.setText(Utils.shipping_phone_num);
        et_shipping_address_1.setText(Utils.shipping_address_1);
        et_shipping_address_2.setText(Utils.shipping_address_2);
        et_shipping_zip_code.setText(Utils.shipping_zip_code);
        et_shipping_city.setText(Utils.shipping_city);
        spinner_shipping_country.setSelection(Utils.shipping_country_position);
        spinner_shipping_state.setSelection(Utils.shipping_state_position);


    }

    private void clickEvent() {
        paynow.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {
                                          Methods.HideKayboard(Shipping_details.this);
                                          getEditTextValues();// calling method

                                      }

                                  }

        );
        spinner_shipping_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Utils.shipping_country_position=arg2;

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        spinner_shipping_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Utils.shipping_state_position=arg2;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    private void getEditTextValues() {

        Utils.shipping_email = et_shipping_email.getText().toString();
        Utils.billing_first_name = et_shipping_first_name.getText().toString();
        Utils.billing_last_name = et_shipping_last_name.getText().toString();
        Utils.shipping_company_name = et_shipping_company_name.getText().toString();
        Utils.shipping_phone_num = et_shipping_phone_num.getText().toString();
        Utils.shipping_address_1 = et_shipping_address_1.getText().toString();
        Utils.shipping_address_2 = et_shipping_address_2.getText().toString();
        Utils.shipping_city = et_shipping_city.getText().toString();
        Utils.shipping_country = spinner_shipping_country.getSelectedItem().toString();
        Utils.shipping_state = spinner_shipping_state.getSelectedItem().toString();
        Utils.shipping_zip_code = et_shipping_zip_code.getText().toString();
        if (Utils.shipping_email.length() == 0 || Utils.billing_first_name.length() == 0 || Utils.billing_last_name.length() == 0 ||
                Utils.shipping_phone_num.length() == 0 || Utils.shipping_address_1.length() == 0 || Utils.shipping_city.length() == 0 || Utils.shipping_country.equals( "CHOOSE COUNTRY" ) ||  Utils.shipping_state.equals( "CHOOSE STATE" ) || Utils.shipping_zip_code.length() == 0) {
            Toast.makeText( getApplicationContext(), "Please enter all the fields.", Toast.LENGTH_LONG ).show();
        } else {
            json_arr = query.getJsonproduct();
            json = new Gson().toJson(json_arr);

            System.out.println("gson=" + json);
            if (!network) {
                Methods.conDialog(Shipping_details.this);
            } else {
                new FetchCountTask().execute();
            }


        }
    }


    private void populate_State_spinner(ArrayList<String> Shipping_sArraylist_state) {


//        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        view = inflater.inflate(R.layout.m_spinner,mLinearLayout, false);
//        mLinearLayout.addView(view);

        Shipping_sArraylist_state.add("CHOOSE STATE");
        Shipping_sArraylist_state.add("Alaska");
        Shipping_sArraylist_state.add("Alabama");
        Shipping_sArraylist_state.add("Arkansas");
        Shipping_sArraylist_state.add("Arizona ");
        Shipping_sArraylist_state.add("California");
        Shipping_sArraylist_state.add("Colorado");
        Shipping_sArraylist_state.add("Connecticut");
        Shipping_sArraylist_state.add("District of Columbia");
        Shipping_sArraylist_state.add("Delaware");
        Shipping_sArraylist_state.add("Florida");
        Shipping_sArraylist_state.add("Georgia");
        Shipping_sArraylist_state.add("Hawaii");
        Shipping_sArraylist_state.add("Iowa");
        Shipping_sArraylist_state.add("Idaho");
        Shipping_sArraylist_state.add("Illinois");
        Shipping_sArraylist_state.add("Indiana");
        Shipping_sArraylist_state.add("Kansas");
        Shipping_sArraylist_state.add("Kentucky");
        Shipping_sArraylist_state.add("Louisiana");
        Shipping_sArraylist_state.add("Massachusetts");
        Shipping_sArraylist_state.add("Maryland");
        Shipping_sArraylist_state.add("Maine");
        Shipping_sArraylist_state.add("Michigan");
        Shipping_sArraylist_state.add("Minnesota");
        Shipping_sArraylist_state.add("Missouri");
        Shipping_sArraylist_state.add("Mississippi");
        Shipping_sArraylist_state.add("Montana");
        Shipping_sArraylist_state.add("North Carolina");
        Shipping_sArraylist_state.add("North Dakota");
        Shipping_sArraylist_state.add("Nebraska");
        Shipping_sArraylist_state.add("New Hampshire");
        Shipping_sArraylist_state.add("New Jersey");
        Shipping_sArraylist_state.add("New Mexico");
        Shipping_sArraylist_state.add("New York");
        Shipping_sArraylist_state.add("Ohio");
        Shipping_sArraylist_state.add("Oklahoma");
        Shipping_sArraylist_state.add("Pennsylvania");
        Shipping_sArraylist_state.add("Rhode Island");
        Shipping_sArraylist_state.add("South Carolina");
        Shipping_sArraylist_state.add("South Dakota");
        Shipping_sArraylist_state.add("Tennessee");
        Shipping_sArraylist_state.add("Texas");
        Shipping_sArraylist_state.add("Utah");
        Shipping_sArraylist_state.add("Virginia");
        Shipping_sArraylist_state.add("Vermont");
        Shipping_sArraylist_state.add("Washington");
        Shipping_sArraylist_state.add("Wisconsin");
        Shipping_sArraylist_state.add("West Virginia");
        Shipping_sArraylist_state.add("Wyoming");


        Spinner drop_down = (Spinner) findViewById(R.id.shipping_detail_choose_state_spinner);

        drop_down.setAdapter(new ArrayAdapter<String>(Shipping_details.this, android.R.layout.simple_dropdown_item_1line, Shipping_sArraylist_state));

    }

    private void populate_Country_spinner(ArrayList<String> Shipping_sArraylist_country) {


//        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        view = inflater.inflate(R.layout.m_spinner,mLinearLayout, false);
//        mLinearLayout.addView(view);

        Shipping_sArraylist_country.add("CHOOSE COUNTRY");
        Shipping_sArraylist_country.add("United States");


        Spinner drop_down = (Spinner) findViewById(R.id.shipping_detail_choose_country_spinner);
        drop_down.setAdapter(new ArrayAdapter<String>(Shipping_details.this, android.R.layout.simple_dropdown_item_1line, Shipping_sArraylist_country));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu, menu);

        MenuItem item = menu.findItem(R.id.notification_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        Utils.setBadgeCount(this, icon, MainActivity.fetch_arr.size());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.notification_cart) {
            if (MainActivity.fetch_arr.size() != 0) {
                Intent intent = new Intent(Shipping_details.this, Pop_Window.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                //     displayPopupWindow();
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), "No Items in Cart", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 144);
                toast.show();
            }

        }

        return true;
    }

    /*
Updates the count of notifications in the ActionBar.
*/
    private void updateNotificationsBadge(int count) {
//        mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }


    /*
    Sample AsyncTask to fetch the notifications count
    */
    class FetchCountTask extends AsyncTask<Void, Void, Integer> {
        String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            progess.show();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            response = shipping_details();
            return null;
        }

        @Override
        public void onPostExecute(Integer count) {
//            updateNotificationsBadge(count);

            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.optString("status").equals("true")) {
                    String data = jobj.optString("Data");
                    SharedPreferences prefs = getSharedPreferences( "Olson", MODE_PRIVATE );
                    if (prefs.getString("pwd","").length()==0) {
                        progess.dismiss();
                        Intent intent = new Intent(getApplicationContext(), Pay_Pal_Activity.class);
                        intent.putExtra("shipping_cost", data);
                        intent.putExtra("json", json);
                        startActivity(intent);
                        finish();
                    }else {
                        new paymentconfermation(data).execute();
                    }
                } else {
                    progess.dismiss();
                    Methods.dialog(Shipping_details.this, "Alert!", "Network Response Error.", android.R.drawable.ic_dialog_info);

                }
            } catch (Exception e) {
                e.printStackTrace();
                progess.dismiss();
                Methods.dialog(Shipping_details.this, "Alert!", "Network Response Error.", android.R.drawable.ic_dialog_info);

            }
        }

    }
    class paymentconfermation extends AsyncTask<String, Void, String> {

        String response;
        String shipping_cost, json;
        double purchased_cost;
        double grand_total;

        public paymentconfermation(String data) {
            this.shipping_cost=data;
            purchased_cost = Pop_Window.purchased_total;
            grand_total = Double.parseDouble(shipping_cost) + purchased_cost;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            response = getdata(shipping_cost,purchased_cost,grand_total);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progess.dismiss();
            //    { "status":"true","order_id":23}
            try {
                JSONObject jobj = new JSONObject(response);
                query.deletecart();
                Intent intent = new Intent(Shipping_details.this, Congratulations.class);
                startActivity(intent);
                finish();
            } catch (Exception e) {
            }
        }
    }

    private String getdata(String shipping_cost, double purchased_cost, double grand_total) {
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("first_name", Utils.billing_first_name)
                .appendQueryParameter("last_name", Utils.billing_last_name)
                .appendQueryParameter("address_line_one", Utils.billing_address_1)
                .appendQueryParameter("address_line_two", Utils.billing_address_2)
                .appendQueryParameter("city", Utils.billing_city)
                .appendQueryParameter("state", Utils.billing_state)
                .appendQueryParameter("country", Utils.billing_country)
                .appendQueryParameter("post_code", Utils.billing_zip_code)
                .appendQueryParameter("delivery_address_line_one", Utils.shipping_address_1)
                .appendQueryParameter("delivery_address_line_two", Utils.shipping_address_2)
                .appendQueryParameter("delivery_order_city", Utils.shipping_city)
                .appendQueryParameter("delivery_order_state", Utils.shipping_state)
                .appendQueryParameter("delivery_order_country", Utils.shipping_country)
                .appendQueryParameter("delivery_order_post_code", Utils.shipping_zip_code)
                .appendQueryParameter("sub_total", String.valueOf(purchased_cost))
                .appendQueryParameter("shipping", shipping_cost)
                .appendQueryParameter("grand_total", String.valueOf(grand_total))
                .appendQueryParameter("product_details", json);

        String url = Utils.get_order;
        String response = parser.getJSONFromUrl(url, builder); // builder will be pases blank

        System.out.println("Response==" + response);
        return response;
    }
    @Override
    protected void onResume() {
        super.onResume();
        MainActivity.fetch_arr = query.getcatproduct();
        updateNotificationsBadge(MainActivity.fetch_arr.size());
    }

    public String shipping_details() {
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("product_details", json)
                .appendQueryParameter("postcode", Utils.shipping_zip_code);

        String url = Utils.get_billing_details;
        Log.i("URL", url);

        String response = parser.getJSONFromUrl(url, builder); // builder will be pases blank
        Log.i("Response", "Get Shipping Detail" + response);
        return response;
    }
}
