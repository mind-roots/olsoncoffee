package com.apps.olsoncoffee;

import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.apps.olsoncoffee.Database.DatabaseQueries;

public class Hot_air_roasting extends AppCompatActivity {

    private int mNotificationsCount = 0;
    DatabaseQueries query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_air_roasting);

        init();
        new FetchCountTask().execute();
    }

    private void init() {

        Toolbar mToolbar = (Toolbar) findViewById(R.id.olson_toolbar);
        setSupportActionBar(mToolbar);
        query = new DatabaseQueries( Hot_air_roasting.this );

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle(" ");
            mToolbar.setNavigationIcon(R.mipmap.ic_back);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu, menu);

        MenuItem item = menu.findItem(R.id.notification_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        Utils.setBadgeCount(this, icon, MainActivity.fetch_arr.size());

        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.notification_cart) {
//            if (MainActivity.fetch_arr.size() != 0) {
                Intent intent = new Intent( Hot_air_roasting.this, Pop_Window.class );
                intent.setFlags( Intent.FLAG_ACTIVITY_NO_ANIMATION );
                startActivity( intent );
                //     displayPopupWindow();
          /*  } else {
                Toast toast = Toast.makeText( getApplicationContext(), "No Items in Cart", Toast.LENGTH_LONG );
                toast.setGravity( Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 144 );
                toast.show();
            }
*/
        }

        return true;
    }

    /*
Updates the count of notifications in the ActionBar.
*/
    private void updateNotificationsBadge(int count) {
        mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }



    /*
  Sample AsyncTask to fetch the notifications count
  */
    class FetchCountTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... params) {
            // example count. This is where you'd
            // query your data store for the actual count.
            return null;
        }

        @Override
        public void onPostExecute(Integer count) {
//            updateNotificationsBadge(count);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainActivity.fetch_arr = query.getcatproduct();
        updateNotificationsBadge( MainActivity.fetch_arr.size() );
    }

}
