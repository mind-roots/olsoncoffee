package com.apps.olsoncoffee;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.olsoncoffee.Database.DatabaseQueries;
import com.apps.olsoncoffee.model.Cart_items_model;
import com.apps.olsoncoffee.model.JSONParser;
import com.apps.olsoncoffee.model.Methods;
import com.apps.olsoncoffee.model.ModelDetail;
import com.apps.olsoncoffee.model.Spinner_model;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Product_Detail extends AppCompatActivity {
    public int number;

    public LinearLayout mLinearLayout, ll_bottom;
    View view;
    DatabaseQueries query;

    private ImageView image_of_product;
    private TextView name_of_product, description_of_product, all_selected_coffees, pop_up_selected_product;
    static String selected_coffee_types, selected_product_name, selected_product_id, selected_product_image, selected_product_description, selected_product_price;
    public static TextView price_of_product;
    private LinearLayout add_to_cart;
    private ProgressDialog progress;
    List<ModelDetail> coffee_pack_array = new ArrayList<>();
    StringBuilder sb;
    List<Spinner_model> ui_spinner_arr = new ArrayList<>();
    public static Menu newmenu;
    static double product_cost;
    static double subtotal;
    boolean network;
    String sku, type;
    View view1, view2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_product__detail );

        init();
        clickEvents();


    }

    private void init() {
        network = Methods.isNetworkConnected( getApplicationContext() );
        Toolbar mToolbar = (Toolbar) findViewById( R.id.olson_toolbar );
        setSupportActionBar( mToolbar );

        query = new DatabaseQueries( Product_Detail.this );

        getSupportActionBar().setDisplayHomeAsUpEnabled( true );
        setTitle( " " );
        mToolbar.setNavigationIcon( R.mipmap.ic_back );

        /*
        * *************************************** getting intent***********************************************
        * ****************************************************************************************************
         */

        Intent intent = getIntent();
        type = intent.getStringExtra( "intent_type" );


        if (!network) {
            Methods.conDialog( Product_Detail.this );
        } else {
            if (type.equals( "main" )) {
                sku = intent.getStringExtra( "SKU" );
                new FetchMonthSpecial().execute();
            } else {
                selected_product_name = intent.getStringExtra( "name" );
                selected_product_id = intent.getStringExtra( "id" );
                new FetchCountTask().execute();
            }
        }
        ll_bottom = (LinearLayout) findViewById( R.id.ll_bottom );
        view1 = (View) findViewById( R.id.view1 );
        view2 = (View) findViewById( R.id.view2 );
        mLinearLayout = (LinearLayout) findViewById( R.id.property );
        name_of_product = (TextView) findViewById( R.id.selected_product_name );
        image_of_product = (ImageView) findViewById( R.id.selected_product_image );
        add_to_cart = (LinearLayout) findViewById( R.id.add_to_cart );
        description_of_product = (TextView) findViewById( R.id.selected_product_description );
        price_of_product = (TextView) findViewById( R.id.item_price );

    }

    private void clickEvents() {
        add_to_cart.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {


//                all_selected_coffees.setText( selected_coffee_types );

                sb = new StringBuilder();
                for (int i = 0; i < ui_spinner_arr.size(); i++) {
                    if (ui_spinner_arr.get( i ).spinner_id.getSelectedItemPosition() > 0) {
                        sb.append( ui_spinner_arr.get( i ).spinner_id.getSelectedItem() ).append( "," );
                    } else {
                        Toast.makeText( Product_Detail.this, "Select all Values", Toast.LENGTH_SHORT ).show();
                        return; // return will exit the control from  method
                    }
                }
                if (sb.length() > 0) {
                    sb.deleteCharAt( sb.lastIndexOf( "," ) );
                }

                Cart_items_model model = new Cart_items_model();
                model.product_icon = selected_product_image;
                model.product_id=selected_product_id;
                model.product_name = selected_product_name;
                model.product_types = sb.toString();
                model.product_quantity = String.valueOf( 1 );
                if (product_cost==0.0){
                    product_cost=Double.parseDouble( selected_product_price );
                }
                model.total_price = product_cost;

                MainActivity.fetch_arr = query.getcatproduct();
                boolean exist = false;
                if (MainActivity.fetch_arr.size() > 0) {
                    for (Cart_items_model item : MainActivity.fetch_arr) {
                        if (item.product_types.equals( model.product_types )&&item.product_id.equals( selected_product_id )) {
                            exist = true;

                        }
                    }
                    if (!exist) {
                        subtotal = product_cost + subtotal;
//                            MainActivity.cart_items.add( model );
                        query.create_cart_items( model );        // adding values to database
                        MainActivity.fetch_arr = query.getcatproduct();
                        number = MainActivity.fetch_arr.size();
                        updateNotificationsBadge( number );

                        Toast toast = Toast.makeText( getApplicationContext(), "Product Added", Toast.LENGTH_SHORT );
                        toast.setGravity( Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 144 );
                        toast.show();
                    } else {
                        Toast toast = Toast.makeText( getApplicationContext(), "No two same products, bag size can be same. Please select the different bag size for this product.", Toast.LENGTH_SHORT );
                        toast.setGravity( Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 144 );
                        toast.show();
                    }
                } else {
                    subtotal = product_cost + subtotal;
//                    MainActivity.cart_items.add( model );
                    query.create_cart_items( model );           // adding values to database
                    MainActivity.fetch_arr = query.getcatproduct();
                    number = MainActivity.fetch_arr.size();
                    updateNotificationsBadge( number );

                    Toast toast = Toast.makeText( getApplicationContext(), "Product Added", Toast.LENGTH_SHORT );
                    toast.setGravity( Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 144 );
                    toast.show();

                }
            }

        } );

    }


    /**********************************************************
     * Inflate spinners layout
     ********************************************************/
    private void InflateLayout(final ModelDetail arr_list, LinearLayout mLinearLayout, int id) {


        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        view = inflater.inflate( R.layout.m_spinner, mLinearLayout, false );
        mLinearLayout.addView( view );

        final Spinner drop_down = (Spinner) view.findViewById( R.id.spinner1 );

        drop_down.setAdapter( new ArrayAdapter<String>( Product_Detail.this, android.R.layout.simple_spinner_dropdown_item, arr_list.value ) );

        //  android.R.layout.simple_dropdown_item_1line
//        selected_coffee_types = drop_down.getSelectedItem().toString() +",";
        drop_down.setId( id );

        drop_down.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override

                    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                        if (drop_down.getId() == 0)
                            for (int i = 0; i < ui_spinner_arr.size(); i++) {
                                if (i > 0) {
                                    ui_spinner_arr.get( i ).spinner_id.setSelection( 0 );
                                }

                            }

                        selected_coffee_types = adapterView.getItemAtPosition( pos ).toString();

                        product_cost = Double.parseDouble( arr_list.price.get( pos ).toString() ) + Double.parseDouble( selected_product_price.toString() );
                        price_of_product.setText( String.valueOf( product_cost ) );
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {


                    }
                }
        );


        try {
            Spinner_model model = new Spinner_model();
            model.spinner_id = drop_down;
            ui_spinner_arr.add( model );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate( R.menu.cart_menu, newmenu );

        MenuItem item = newmenu.findItem( R.id.notification_cart );
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        //MainActivity.cart_items=query.getcatproduct();
        // Update LayerDrawable's BadgeDrawable
        Utils.setBadgeCount( this, icon, MainActivity.fetch_arr.size() );

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.notification_cart) {
            // MainActivity.cart_items=query.getcatproduct();

            Intent intent = new Intent( Product_Detail.this, Pop_Window.class );
            intent.setFlags( Intent.FLAG_ACTIVITY_NO_ANIMATION );
            startActivity( intent );
            //     displayPopupWindow();


        }

        return true;
    }
    /*
    * Updates the count of notifications in the ActionBar.
    */

    public void updateNotificationsBadge(int count) {
        //   mNotificationsCount = count;

        invalidateOptionsMenu();
    }


    /*
  Sample AsyncTask to fetch the notifications count
  */
    class FetchCountTask extends AsyncTask<Void, Void, Integer> {
        String status;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog( Product_Detail.this );
            progress.setIndeterminate( true );
            progress.setMessage( "Fetching" );
            progress.setCancelable( false );
            progress.show();
        }

        @Override
        protected Integer doInBackground(Void... params) {

            String response = products_detail( selected_product_id );

            try {
                JSONObject jsonObject = new JSONObject( response );
                status = jsonObject.optString( "status" );
                if (status.equals( "true" )) {
                    try {


                    JSONArray jsonArray = jsonObject.getJSONArray( "Data" );

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject( i );

                        selected_product_image = obj.optString( "image" );
                        selected_product_description = obj.optString( "description" );
                        selected_product_price = obj.optString( "sale_price" );
                        try {
                            JSONObject rules = obj.optJSONObject( "rules" );
                            Iterator<String> iter = rules.keys();
                            while (iter.hasNext()) {

                                String key = iter.next();
                                ModelDetail item = new ModelDetail();
                                //item.key = key;

                                item.value = new ArrayList<>(); // instantiating the List declared in model classs
                                item.price = new ArrayList<>();
                                item.value.add( key );
                                item.price.add( "0.00" );
                                ;
                                JSONArray jarr = rules.getJSONArray( key );
                                for (int j = 0; j < jarr.length(); j++) {

                                    String data = jarr.optString( j );
                                    ///////////////////////Splitting spinner values ///////////////////////////////////////////////////////////////
                                    if (data.contains( "+" )) {
                                        String[] separated = data.split( "\\+" );
                                        data = separated[0];
                                        String price = separated[1];
                                        item.value.add( data );
                                        item.price.add( price );
                                        //////////////////////////////////////////////////////////////////////////////////////
                                    } else {
                                        item.value.add( data );
                                        item.price.add( "0.00" );
                                    }
                                }

                                coffee_pack_array.add( item );

                                Log.v( "Response", "Coffee Pack Array" + coffee_pack_array );
                                Log.v( "Response", "Price Array Size:" + item.price.size() );

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    }catch (Exception e){}
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return number;
        }

        @Override
        public void onPostExecute(Integer count) {
            super.onPostExecute( count );
            progress.dismiss();
            try {

                if (status.equals( "true" )) {
                    ll_bottom.setVisibility( View.VISIBLE );
                    view2.setVisibility( View.VISIBLE );
                    view1.setVisibility( View.VISIBLE );
                    for (int j = 0; j < coffee_pack_array.size(); j++) {

                        InflateLayout( coffee_pack_array.get( j ), mLinearLayout, j );

                    }
                    name_of_product.setText( selected_product_name );

                    Picasso.with( Product_Detail.this )
                            .load( selected_product_image )
//                .resize(650, 200)
                            .into( image_of_product );

                    description_of_product.setText( selected_product_description );

                    price_of_product.setText( selected_product_price );
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progress != null)
            progress.dismiss();
    }

    public String products_detail(String id) {
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter( "", "" );

        String url = Utils.get_product_details + "&product_id=" + id;

        String response = parser.getJSONFromUrl( url, builder ); // builder will be pases blank
        Log.i( "Response", "Get Products Detail" + response );
        return response;
    }


    class FetchMonthSpecial extends AsyncTask<Void, Void, Void> {
        String status;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog( Product_Detail.this );
            progress.setIndeterminate( true );
            progress.setMessage( "Fetching" );
            progress.setCancelable( false );
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String response = months_specialproducts_detail( sku );

            try {
                JSONObject jsonObject = new JSONObject( response );
                status = jsonObject.optString( "status" );
                if (status.equals( "true" )) {
                    JSONArray jsonArray = jsonObject.getJSONArray( "Data" );

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject( i );

                        selected_product_image = obj.getString( "image" );
                        selected_product_description = obj.getString( "description" );
                        selected_product_price = obj.getString( "sale_price" );
                        try {
                            JSONObject rules = obj.optJSONObject( "rules" );
                            Iterator<String> iter = rules.keys();
                            while (iter.hasNext()) {

                                String key = iter.next();
                                ModelDetail item = new ModelDetail();
                                //item.key = key;

                                item.value = new ArrayList<>(); // instantiating the List declared in model classs
                                item.price = new ArrayList<>();
                                item.value.add( key );
                                item.price.add( "0.00" );
                                ;
                                JSONArray jarr = rules.getJSONArray( key );
                                for (int j = 0; j < jarr.length(); j++) {

                                    String data = jarr.optString( j );
                                    ///////////////////////Splitting spinner values ///////////////////////////////////////////////////////////////
                                    if (data.contains( "+" )) {
                                        String[] separated = data.split( "\\+" );
                                        data = separated[0];
                                        String price = separated[1];
                                        item.value.add( data );
                                        item.price.add( price );
                                        //////////////////////////////////////////////////////////////////////////////////////
                                    } else {
                                        item.value.add( data );
                                        item.price.add( "0.00" );
                                    }
                                }

                                coffee_pack_array.add( item );

                                Log.v( "Response", "Coffee Pack Array" + coffee_pack_array );
                                Log.v( "Response", "Price Array Size:" + item.price.size() );
                            }
                        } catch (Exception e) {
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute( aVoid );
            progress.dismiss();
            if (status.equals( "true" )) {
                ll_bottom.setVisibility( View.VISIBLE );
                view2.setVisibility( View.VISIBLE );
                view1.setVisibility( View.VISIBLE );
                for (int j = 0; j < coffee_pack_array.size(); j++) {
                    InflateLayout( coffee_pack_array.get( j ), mLinearLayout, j );
                }
                name_of_product.setText( selected_product_name );

                Picasso.with( Product_Detail.this )
                        .load( selected_product_image )
//                .resize(650, 200)
                        .into( image_of_product );

                description_of_product.setText( selected_product_description );
                price_of_product.setText( selected_product_price );
            }

        }

    }


    public String months_specialproducts_detail(String sku) {
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter( "", "" );

        String url = Utils.get_months_special + "&product_sku=" + sku;

        String response = parser.getJSONFromUrl( url, builder ); // builder will be pases blank
        Log.i( "Response", "Get months special product Detail" + response );
        return response;
    }


    @Override
    protected void onResume() {
        super.onResume();
        MainActivity.fetch_arr = query.getcatproduct();
        updateNotificationsBadge( MainActivity.fetch_arr.size() );
    }
}
