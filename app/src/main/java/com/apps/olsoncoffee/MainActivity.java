package com.apps.olsoncoffee;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.olsoncoffee.Database.DatabaseQueries;
import com.apps.olsoncoffee.model.Cart_items_model;
import com.apps.olsoncoffee.model.Front_image_model;
import com.apps.olsoncoffee.model.JSONParser;
import com.apps.olsoncoffee.model.Methods;
import com.apps.olsoncoffee.model.product_model;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    LinearLayout ll_coffees,ll_perfect_brew,ll_hot_air_roasting,ll_specials,ll_accessories,ll_wholesale;
    RelativeLayout ll_1;
    public static Menu newmenu;
    Product_Detail p_d;
    public static ArrayList<Cart_items_model> fetch_arr = new ArrayList<>();

    ProgressDialog progress;
    String front_image;
    String SKU;
    ImageView myimg;
    public int style;
    boolean network;
//    public static String first_name, last_name, address_line_one, address_line_two, city, state, country, post_code;
//    public static String delivery_address_line_one, delivery_address_line_two, delivery_order_city, delivery_order_state, delivery_order_country, delivery_order_post_code;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    DatabaseQueries queries;
    String exist;
    ProgressBar progress_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );


        updateNotificationsBadge( fetch_arr.size() );
        init();
        Click_events();

        if (!network) {
            Methods.conDialog( MainActivity.this );
        } else {
            new FetchData().execute();
        }


        if (Build.VERSION.SDK_INT < 21) {
            //   style = Color.TRANSPARENT;
            style = R.style.DialogTheme;
        } else {
            style = R.style.DialogTheme1;
        }


    }




    /**************************************
     * Initialize UI elements
     ************************************/
    private void init() {
        prefs = getSharedPreferences( "Olson", MODE_PRIVATE );
        exist = prefs.getString( "Exist", "" );
        if (exist.length() == 0) {
            WholesaleDialog();
        }
        Toolbar mToolbar = (Toolbar) findViewById( R.id.olson_toolbar );
        setSupportActionBar( mToolbar );
        network = Methods.isNetworkConnected( getApplicationContext() );
        queries = new DatabaseQueries( MainActivity.this );

        try {
            getSupportActionBar().setTitle( " " );
        } catch (Exception e) {
            e.printStackTrace();
        }
        ll_coffees = (LinearLayout) findViewById( R.id.ll_coffees );
        ll_perfect_brew = (LinearLayout) findViewById( R.id.ll_3_3 );
        ll_hot_air_roasting = (LinearLayout) findViewById( R.id.ll_4_1 );
        ll_specials = (LinearLayout) findViewById( R.id.ll_specials );
        ll_accessories = (LinearLayout) findViewById( R.id.ll_Accessories );
        ll_wholesale = (LinearLayout) findViewById( R.id.ll_wholesale );
        ll_1 = (RelativeLayout) findViewById( R.id.ll_1 );
        progress_bar = (ProgressBar)findViewById( R.id.progress_front );

        myimg = (ImageView) findViewById( R.id.coffe_pack );

        p_d = new Product_Detail();
    }

    private void WholesaleDialog() {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder( MainActivity.this, style );
        builder.setMessage( "If you are an Olson Coffee House Wholesaler please click on the" +
                " wholesale section below and put in your password to process wholesale orders  " );

        builder.setCancelable( false );
        builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialog, int which) {
                edit = prefs.edit();
                edit.putString( "Exist", "exist" );
                edit.commit();
                dialog.dismiss();

            }
        } );

        builder.show();
    }
    /**************************************
     * Click events
     ************************************/
    private void Click_events() {

        ll_coffees.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( getApplicationContext(), ProductList.class );
                intent.putExtra( "category_type", "coffee" );
                startActivity( intent );

            }
        } );
        ll_specials.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( getApplicationContext(), ProductList.class );
                intent.putExtra( "category_type", "specials" );
                startActivity( intent );
            }
        } );

        ll_accessories.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( getApplicationContext(), ProductList.class );
                intent.putExtra( "category_type", "Accessories" );
                startActivity( intent );

            }
        } );

        ll_wholesale.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (prefs.getString( "pwd", "" ).length() == 0) {
                    final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder( MainActivity.this, style );
                    builder.setTitle( "Please enter the password" );
//                builder.setMessage( "Edit the Template Name and tap on Update" );
                    LayoutInflater inflater = LayoutInflater.from( getApplicationContext() );
                    view = inflater.inflate( R.layout.edittext, null );

                    final EditText input = (EditText) view.findViewById( R.id.wholesaler_pass );
                    input.requestFocus();
//                input.setHint( "Enter Template Name" );
                /*input.setText( template_name );
                input.setSelection( template_name.length() );*/
                    input.setTextColor( Color.BLACK );

                    builder.setView( view );
                    builder.setCancelable( false );
                    builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(final DialogInterface dialog, int which) {
                            String data = input.getText().toString();
                            if (data.equals( "1234" ) && prefs.getString( "pwd", "" ).length() == 0) {
                                edit = prefs.edit();
                                edit.putString( "pwd", "1234" );
                                edit.commit();
                                Intent intent = new Intent( getApplicationContext(), ProductList.class );
                                intent.putExtra( "category_type", "wholesale" );
                                startActivity( intent );

                            } else if (data.length() == 0) {
                                Toast.makeText( MainActivity.this, "Please enter password", Toast.LENGTH_SHORT ).show();
                            } else {
                                Toast.makeText( MainActivity.this, "Incorrect Password", Toast.LENGTH_SHORT ).show();
                            }

                        }
                    } );
                    builder.setNegativeButton( "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    } );
                    builder.show();
                } else {
                    Intent intent = new Intent( getApplicationContext(), ProductList.class );
                    intent.putExtra( "category_type", "wholesale" );
                    startActivity( intent );
                }

            }

                /*Intent intent = new Intent( getApplicationContext(), ProductList.class );
                intent.putExtra( "category_type", "wholesale" );
                startActivity( intent );*/

        } );

        ll_1.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( getApplicationContext(), Product_Detail.class );
                intent.putExtra( "SKU", SKU );
                intent.putExtra( "intent_type", "main" );
                startActivity( intent );

            }
        } );

        ll_perfect_brew.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( getApplicationContext(), The_perfect_brew.class );
                startActivity( intent );

            }
        } );

        ll_hot_air_roasting.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( getApplicationContext(), Hot_air_roasting.class );
                startActivity( intent );
            }
        } );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate( R.menu.cart_menu, newmenu );

        MenuItem item = newmenu.findItem( R.id.notification_cart );
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        //MainActivity.cart_items = queries.getcatproduct();
        // Update LayerDrawable's BadgeDrawable
        Utils.setBadgeCount( this, icon, MainActivity.fetch_arr.size() );

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            if (item.getItemId() == R.id.notification_cart) {
                Intent intent = new Intent( MainActivity.this, Pop_Window.class );
                intent.setFlags( Intent.FLAG_ACTIVITY_NO_ANIMATION );
                startActivity( intent );
                //     displayPopupWindow();


            }
        }

        return true;
    }

    public void updateNotificationsBadge(int count) {
        //   mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainActivity.fetch_arr = queries.getcatproduct();
        updateNotificationsBadge( fetch_arr.size() );
    }

    public String get_front_image() {
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter( "", "" );

        String url = Utils.get_front_image;

        String response = parser.getJSONFromUrl( url, builder ); // builder will be pases blank
        Log.i( "Response", "Get front Image" + response );
        return response;
    }

    class FetchData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progress = new ProgressDialog( MainActivity.this );
            progress.setIndeterminate( true );
            progress.setMessage( "Fetching" );
            progress.setCancelable( false );
            progress.show();*/
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String response = get_front_image();

            try {
                JSONObject obj = new JSONObject( response );
                JSONArray jarr = obj.getJSONArray( "Data" );

                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jobj = jarr.getJSONObject( i );
                    front_image = jobj.optString( "image" );
                    SKU = jobj.optString( "SKU" );

                    Log.v( "Response", "Coffee Pack image" + front_image );

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute( aVoid );


            Picasso.with( MainActivity.this )
                    .load( front_image )
//                .resize(650, 200)
                    .into( myimg, new Callback() {
                        @Override
                        public void onSuccess() {
                            progress_bar.setVisibility( View.GONE );
                        }

                        @Override
                        public void onError() {
                            progress_bar.setVisibility( View.GONE );
                        }
                    } );

//            progress.dismiss();
        }
    }
}

