package com.apps.olsoncoffee;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.olsoncoffee.Database.DatabaseQueries;
import com.apps.olsoncoffee.model.Cart_items_model;
import com.apps.olsoncoffee.model.ModelCartArray;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Manifest;

/**
 * Created by user on 17/5/16.
 */
public class Pop_Window extends AppCompatActivity {
    // int mNotificationsCount;


    private PopupWindow pwindo;
    private TextView all_selected_coffees, pop_up_selected_product, purchasingItems, purchasing_items_total, purchansing_items_subtotal;
    private Cart_adapter mAdapter;
    private ImageView checkout;
    private RecyclerView mRecyclerView;
    public static Menu newmenu;
    public static double purchased_total = 0.00;
    double myLastTotal;
    double myLastSubTotal;
    DatabaseQueries query;
    public int no_of_items;
    LinearLayout item_layout;
    RelativeLayout no_tem;
    ArrayList<Cart_items_model> prod_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_pop );

        init();

    }


    private void init() {

        Toolbar mToolbar = (Toolbar) findViewById( R.id.olson_toolbar );
        setSupportActionBar( mToolbar );
        updateNotificationsBadge( MainActivity.fetch_arr.size() );

        query = new DatabaseQueries( Pop_Window.this );

//        try {
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );
        setTitle( " " );
        mToolbar.setNavigationIcon( R.mipmap.ic_back );

        //  displayPopupWindow();

//        all_selected_coffees = (TextView) findViewById( R.id.pop_up_all_selected_coffees );
//        pop_up_selected_product = (TextView) findViewById( R.id.pop_selected_product_name );
        checkout = (ImageView) findViewById( R.id.check_out_text_view );
        item_layout = (LinearLayout) findViewById( R.id.item_layout );
        no_tem = (RelativeLayout) findViewById( R.id.no_tem );
        purchansing_items_subtotal = (TextView) findViewById( R.id.purchasing_items_subtotal );
        purchasing_items_total = (TextView) findViewById( R.id.purchasing_items_total );
        purchasingItems = (TextView) findViewById( R.id.no_of_items );
        mRecyclerView = (RecyclerView) findViewById( R.id.my_recycler_view_pop_up );


        final LinearLayoutManager layoutManager = new LinearLayoutManager( Pop_Window.this );
        layoutManager.setOrientation( LinearLayoutManager.VERTICAL );
        mRecyclerView.setLayoutManager( layoutManager );

        /*
        * *************************************************************
        * Fetching Values from Database
        * *****************    START   ********************************
         */

        MainActivity.fetch_arr = query.getcatproduct();

        if (MainActivity.fetch_arr.size() > 0) {
            no_tem.setVisibility( View.GONE );
            item_layout.setVisibility( View.VISIBLE );
        } else {
            no_tem.setVisibility( View.VISIBLE );
            item_layout.setVisibility( View.GONE );
        }
        // ****************  END  ***********************************
        purchased_total = 0.00;
        no_of_items = 0;
        for (Cart_items_model item : MainActivity.fetch_arr) {
            purchased_total = (double) purchased_total + ((double) item.total_price) * (Double.parseDouble( item.product_quantity ));
            no_of_items = no_of_items + Integer.parseInt( item.product_quantity );
        }

        purchasingItems.setText( String.valueOf( no_of_items ) );
        purchasing_items_total.setText( new DecimalFormat( "##.##" ).format( purchased_total ) );
        purchansing_items_subtotal.setText( new DecimalFormat( "##.##" ).format( purchased_total ) );


        mAdapter = new Cart_adapter( Pop_Window.this, MainActivity.fetch_arr ); // passing values to adapter

        mRecyclerView.setAdapter( mAdapter );
        Cart_items_model model = new Cart_items_model();

        purchansing_items_subtotal.setText( String.valueOf( String.format( "%.2f", purchased_total ) ) );
        purchasing_items_total.setText( String.valueOf( String.format( "%.2f", purchased_total ) ) );
        MainActivity.fetch_arr = query.getcatproduct();
//        no_of_items = MainActivity.fetch_arr.size() ;
//        no_of_items = Integer.parseInt( no_of_items +model.product_quantity );
//                purchasingItems.setText( String.valueOf( no_of_items ) );


        // calling method
        popUpClickEvents();
    }


    /*public void displayPopupWindow() {
        // We need to get the instance of the LayoutInflater

        LayoutInflater inflater = (LayoutInflater) Pop_Window.this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        final View layout = inflater.inflate( R.layout.popup_content, (ViewGroup) findViewById( R.id.custom_pop_up_layout ) );


        Display display = ((WindowManager) getSystemService( Context.WINDOW_SERVICE )).getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();

        pwindo = new PopupWindow( layout, width, 1200, true );


        pwindo.showAtLocation( layout, Gravity.TOP, 0, 140 ); // 130 is top margin


    }*/

    /***********************************************************************
     * Pop Up click events
     *********************************************************************/
    private void popUpClickEvents() {

        checkout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prod_list = query.getcatproduct();

                if (prod_list.size() > 1) {
                    Intent intent = new Intent( getApplicationContext(), Billing_details.class );
                    startActivity( intent );
                    finish();
                } else {
                    for (Cart_items_model model : prod_list) {
                        if (model.product_types.contains( "12 oz" ) && model.product_quantity.equals( "1" )) {
                            dialog( "Alert!", "You need to place a minimum order of atleast two 12oz bags.", android.R.drawable.ic_dialog_info, 0 );


                        } else {
                            Intent intent = new Intent( getApplicationContext(), Billing_details.class );
                            startActivity( intent );
                            finish();
                        }
                    }
                }
            }

        } );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate( R.menu.cart_menu, newmenu );

        MenuItem item = newmenu.findItem( R.id.notification_cart );
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        Utils.setBadgeCount( this, icon, MainActivity.fetch_arr.size() );

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.notification_cart) {
            finish();
            overridePendingTransition( 0, 0 );

        }

        return true;
    }


    /*
     *  ************************************************************
     *  Updates the count of notifications in the ActionBar.
     *  ************************************************************
    */

    public void updateNotificationsBadge(int count) {
        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed() {
        //  super.onBackPressed();
    }


//--------------------------ADAPTER CLASS---------------------------//

    public class Cart_adapter extends RecyclerView.Adapter<Cart_adapter.CustomViewHolder> {

        private Activity context;
        public ArrayList<Cart_items_model> mData = new ArrayList<>();
        LayoutInflater inflater;
        int d = 0;
        Cart_items_model model = new Cart_items_model();

        public Cart_adapter(Activity context, ArrayList<Cart_items_model> data) {

            this.mData = data;
            this.context = context;
            inflater = LayoutInflater.from( context );


        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = inflater.inflate( R.layout.cart_items_custom_list, parent, false );

            CustomViewHolder viewHolder = new CustomViewHolder( v );

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final CustomViewHolder holder, final int position) {


            holder.textView_product_name.setText( mData.get( position ).product_name );
            holder.textView_product_types.setText( mData.get( position ).product_types );
            holder.textView_product_quantity.setText( mData.get( position ).product_quantity );

            holder.single_item_price = mData.get( position ).total_price;
            holder.textView_sum.setText( String.valueOf( holder.single_item_price ) );
            holder.cancel_pop_up.setTag( position );
         /*   if (d == 0) {
d=1;
                purchased_total = (double) purchased_total + ((double) mData.get(position).total_price) * (Double.parseDouble(mData.get(position).product_quantity));
                no_of_items = no_of_items + Integer.parseInt(mData.get(position).product_quantity);
                purchasingItems.setText(String.valueOf(no_of_items));
                purchasing_items_total.setText(new DecimalFormat("##.##").format(purchased_total));
                purchansing_items_subtotal.setText(new DecimalFormat("##.##").format(purchased_total));

            }
*/
            Picasso.with( context )
                    .load( mData.get( position ).product_icon )
                    .resize( 100, 100 )
                    .into( holder.imageView_product_icon );


            holder.cancel_pop_up.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog( "Alert!", "Are you sure, you want to delete this product.", android.R.drawable.ic_dialog_info, position );


                }
            } );


            holder.increment.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    d = 1;
                    int number_of_packs = Integer.parseInt( mData.get( position ).product_quantity );
                    number_of_packs++;

                    // changing decrement icon when product quantity is greater than 1
                    if (number_of_packs > 1) {
                        holder.decrement.setImageResource( R.mipmap.sort_down_black );
                    } else {
                        holder.decrement.setImageResource( R.mipmap.sort_down_gray );
                    }
                    /////////////////////////////////////////////////////////////////////

                    mData.get( position ).product_quantity = String.valueOf( number_of_packs );
                    no_of_items = no_of_items + 1;
                    purchasingItems.setText( String.valueOf( no_of_items ) );

                    holder.decrement.setEnabled( true );
                    // set number of packs to textview
                    holder.textView_product_quantity.setText( String.valueOf( number_of_packs ) );
                    holder.total = mData.get( position ).total_price * Double.parseDouble( mData.get( position ).product_quantity );
                    // calculating total
                    purchased_total = purchased_total + mData.get( position ).total_price;

                    purchasing_items_total.setText( "" + new DecimalFormat( "##.##" ).format( purchased_total ) );
                    purchansing_items_subtotal.setText( "" + new DecimalFormat( "##.##" ).format( purchased_total ) );

                    model.product_quantity = String.valueOf( number_of_packs );
                    model.id = mData.get( position ).id;

                    query.updatecart( model );


                }
            } );

            holder.decrement.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int number_of_packs = Integer.parseInt( mData.get( position ).product_quantity );
                    if (number_of_packs == 1) {
                        holder.decrement.setEnabled( false );
                    } else {
                        number_of_packs--;

                        // changing decrement icon when product quantity is greater than 1
                        if (number_of_packs > 1) {
                            holder.decrement.setImageResource( R.mipmap.sort_down_black );
                        } else {
                            holder.decrement.setImageResource( R.mipmap.sort_down_gray );
                        }
                        /////////////////////////////////////////////////////////////////////

                        mData.get( position ).product_quantity = String.valueOf( number_of_packs );
                        // set number of packs to textview
                        holder.textView_product_quantity.setText( String.valueOf( mData.get( position ).product_quantity ) );
                        no_of_items = no_of_items - 1;
                        purchasingItems.setText( String.valueOf( no_of_items ) );
                        purchased_total = purchased_total - mData.get( position ).total_price;
                        purchasing_items_total.setText( "" + new DecimalFormat( "##.##" ).format( purchased_total ) );
                        purchansing_items_subtotal.setText( "" + new DecimalFormat( "##.##" ).format( purchased_total ) );

                        holder.textView_sum.setText( String.valueOf( holder.total ) );

                          /*
                     * ***************************************************
                     * getting value to update in database
                     * ****************************************************
                     */
                        model.product_quantity = String.valueOf( number_of_packs );
                        model.id = mData.get( position ).id;
                       /* model.total_price = purchased_total;
                        model.subtotal = purchased_total;
                        model.no_of_products = no_of_items;*/
//                        purchasingItems.setText( String.valueOf( no_of_items ) );

                        query.updatecart( model );
                    /*
                     * ******************************************************
                     * CLose
                     * ******************************************************
                     */

                    }
                }

            } );
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            public ImageView imageView_product_icon;
            public TextView textView_product_name, textView_product_types, textView_product_quantity, textView_sum;
            public LinearLayout mLinearLayout_customList;
            private ImageView increment;
            private ImageView decrement;
            private ImageView cancel_pop_up;

            Cart_items_model model;
            double total;
            public double single_item_price;


            public CustomViewHolder(View view) {
                super( view );
                this.imageView_product_icon = (ImageView) view.findViewById( R.id.pop_up_product_icon );
                this.textView_product_name = (TextView) view.findViewById( R.id.pop_selected_product_name );
                this.textView_product_types = (TextView) view.findViewById( R.id.pop_up_all_selected_coffees );
                this.textView_product_quantity = (TextView) view.findViewById( R.id.pop_up_number_of_items );
                this.increment = (ImageView) view.findViewById( R.id.increase_item );
                this.decrement = (ImageView) view.findViewById( R.id.decrease_item );
                this.cancel_pop_up = (ImageView) view.findViewById( R.id.cancel_pop_up );
                this.textView_sum = (TextView) view.findViewById( R.id.sum );


                this.mLinearLayout_customList = (LinearLayout) view.findViewById( R.id.popup_layout );

                model = new Cart_items_model();

            }

        }

        /**********************************************
         * Delete thread dialog
         ********************************************/
        public void dialog(String title, String msg, int icon, final int pos) {
            new AlertDialog.Builder( Pop_Window.this, R.style.AppTheme_Dialog )
                    .setTitle( title )
                    .setMessage( msg )
                    .setPositiveButton( android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deleteMessage( pos );
                        }
                    } )
                    .setNegativeButton( android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    } )

                    .setIcon( icon )
                    .show();

        }

        private void deleteMessage(int position) {
            d = 1;
            query.deletitem( mData.get( position ).product_types, mData.get( position ).product_id ); // deleting item from database
            notifyItemRemoved( position );
            notifyItemRangeChanged( position, mData.size() );
            notifyDataSetChanged();

            MainActivity.fetch_arr = query.getcatproduct();
            no_of_items = no_of_items - Integer.parseInt( mData.get( position ).product_quantity );
            purchasingItems.setText( String.valueOf( no_of_items ) );


            if (MainActivity.fetch_arr.size() > 0) {
                no_tem.setVisibility( View.GONE );
                item_layout.setVisibility( View.VISIBLE );
            } else {
                no_tem.setVisibility( View.VISIBLE );
                item_layout.setVisibility( View.GONE );
            }
            purchased_total = purchased_total - ((Double.parseDouble( mData.get( position ).product_quantity )) * (mData.get( position ).total_price));


            String pur_total = String.format( "%.2f", purchased_total );
//                    String pur_subtotal = String.format( "%.2f", purchased_total );
            // purchansing_items_subtotal.setText( String.valueOf( Product_Detail.subtotal + purchased_total ) );
            purchasing_items_total.setText( "" + pur_total );
            purchansing_items_subtotal.setText( "" + pur_total );
//


            updateNotificationsBadge( mData.size() );
//                    purchasingItems.setText( String.valueOf( mData.size() ) );
            mData.remove( position );  // this shud be in the last of method
        }
    }

    /**********************************************
     * Simple dialog
     ********************************************/
    public void dialog(String title, String msg, int icon, final int pos) {
        new AlertDialog.Builder( Pop_Window.this, R.style.AppTheme_Dialog )
                .setTitle( title )
                .setMessage( msg )
                .setPositiveButton( android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                } )


                .setIcon( icon )
                .show();

    }
}
