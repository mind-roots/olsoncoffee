package com.apps.olsoncoffee.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps.olsoncoffee.Product_Detail;
import com.apps.olsoncoffee.R;
import com.apps.olsoncoffee.model.product_model;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by user on 5/5/16.
 */
public class products_adapter extends RecyclerView.Adapter<products_adapter.CustomViewHolder> {

    private Activity context;
    public List<product_model> mData;
    LayoutInflater inflater;
    public String selected_product_name;
    product_model mProduct_model;


    public products_adapter(Activity context, List<product_model> data) {

        this.mData = data;
        this.context = context;
        inflater = LayoutInflater.from( context );

    }

    public products_adapter() {


    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = inflater.inflate( R.layout.custom_list_products, parent, false );

        CustomViewHolder viewHolder = new CustomViewHolder( v );
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {

        final product_model model = mData.get( position );
        holder.textView.setText( model.product_name );

        Picasso.with( context ).
                load( model.product_image ).resize( 150, 150 )
                .into( holder.imageView, new Callback() {

                    @Override
                    public void onSuccess() {
                        holder.progress.setVisibility( View.GONE );
                    }

                    @Override
                    public void onError() {
                        holder.progress.setVisibility( View.GONE );
                    }
                } );

//        Picasso.with(context)
//                .load(model.product_image)
//                .resize(150, 150)
//                .into(holder.imageView);

/*
 *******************  recycler view on onItemClickListener*********************************************
 *******************************************************************************************************
 */

        holder.mLinearLayout_customList.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected_product_name = model.product_name;
                Intent intent = new Intent( context, Product_Detail.class );
                intent.putExtra( "name", selected_product_name );
                intent.putExtra( "id", model.product_id );
                intent.putExtra( "image", model.product_image );
                intent.putExtra( "intent_type","adapter" );
                context.startActivity( intent );

            }
        } );


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public LinearLayout mLinearLayout_customList;
        ProgressBar progress;

        public CustomViewHolder(View view) {
            super( view );
            this.imageView = (ImageView) view.findViewById( R.id.product_image );
            this.textView = (TextView) view.findViewById( R.id.product_name );
            this.progress = (ProgressBar) view.findViewById( R.id.progress );
            this.mLinearLayout_customList = (LinearLayout) view.findViewById( R.id.ll_custom_list );


        }
    }
}
