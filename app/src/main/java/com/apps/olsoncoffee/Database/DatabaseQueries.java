package com.apps.olsoncoffee.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.apps.olsoncoffee.model.Cart_items_model;
import com.apps.olsoncoffee.model.ModelCartArray;
import com.apps.olsoncoffee.model.product_model;

import java.io.IOException;
import java.util.ArrayList;


public class DatabaseQueries {


    public static DataBaseHelper dataBaseHelper;

    //***************Table names************
    public static String TABLE_PRODUCTS = "product";
    public static String TABLE_CART_ITEMS = "cart_items";


    // 1*********** TABLE_PRODUCTS********
    String PRODUCT_NAME = "product_name";
    String PRODUCT_IMAGE = "product_image";
    String PRODUCT_ICON = "icon";
    String PRODUCT_ID = "product_id";
    String PRODUCT_DESC = "product_description";
    String PRODUCT_SALE_PRICE = "product_sale_price";


    // 1*********** TABLE_CART_ITEMS********
    String C_ID = "id";
    String C_PRODUCT_ID = "product_id";
    String C_PRODUCT_ICON = "product_icon";
    String C_PRODUCT_NAME = "product_name";
    String C_PRODUCT_TYPES = "product_types";
    String C_PRODUCT_QUANTITY = "product_quantity";
    String C_TOTAL_PRICE = "total_price";
    String C_SUBTOTAL = "subtotal";
    String C_N0_OF_PRODUCTS = "no_of_products";


    public DatabaseQueries(Context context) {

        dataBaseHelper = new DataBaseHelper(context);
    }

    private String populateItem(Cursor c, String COLUMN) {

        String item = null;
        item = c.getString(c.getColumnIndex(COLUMN));
        return item;
    }


    //*****************Content Values for view_allfolders*************
    private ContentValues populate_cart_items(Cart_items_model model) {

        ContentValues values = new ContentValues();
        values.put(C_PRODUCT_ICON, model.product_icon);
        values.put(C_PRODUCT_ID, model.product_id);
        values.put(C_PRODUCT_NAME, model.product_name);
        values.put(C_PRODUCT_TYPES, model.product_types);
        values.put(C_PRODUCT_QUANTITY, model.product_quantity);
        values.put(C_TOTAL_PRICE, model.total_price);
        values.put(C_SUBTOTAL, model.subtotal);
        values.put(C_N0_OF_PRODUCTS, model.no_of_products);
//        values.put( C_PRODUCT_ID,model.id );

        return values;
    }


    // /*****************Content Values for view_allfolders*************

    private ContentValues populate_product(product_model model) {

        ContentValues values = new ContentValues();
        values.put(PRODUCT_NAME, model.product_name);
        values.put(PRODUCT_IMAGE, model.product_image);
        values.put(PRODUCT_ICON, model.icon);
        values.put(PRODUCT_ID, model.product_id);
        values.put(PRODUCT_DESC, model.product_description);
        values.put(PRODUCT_SALE_PRICE, model.product_sale_price);

        return values;
    }


    //**********************INSERT into TABLE_CART_ITEMS Table*****************

    public void create_cart_items(Cart_items_model model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populate_cart_items(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_CART_ITEMS, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //**********************INSERT into TABLE_PRODUCTS Table*****************

    public void create_product(product_model model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populate_product(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_PRODUCTS, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ///*****************delete row from TABLE_CROPIMAGE *******************************

    public void deletitem(String type, String productid) {
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        db.delete(TABLE_CART_ITEMS, C_PRODUCT_TYPES + " = '" + type + "' AND " + C_PRODUCT_ID + "='" + productid + "'", null);
    }

//
//    // *******check for data available or not in the table****************************************
//    public boolean checkForTables(){
//        boolean hasTables = false;
//        SQLiteDatabase db =  dataBaseHelper.getReadableDatabase();
//        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_ALLFOLDERS, null);
//
//        if(cursor != null && cursor.getCount() > 0){
//            hasTables=true;
//            cursor.close();
//        }
//
//        return hasTables;
//    }


    public ArrayList<Cart_items_model> getcatproduct() {

        ArrayList<Cart_items_model> arr = new ArrayList<Cart_items_model>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_CART_ITEMS;

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {

                Cart_items_model all = new Cart_items_model();
                all.id = c.getInt(c.getColumnIndex(C_ID));
                all.product_id = c.getString(c.getColumnIndex(C_PRODUCT_ID));
                all.product_name = c.getString(c.getColumnIndex(C_PRODUCT_NAME));
                all.product_quantity = c.getString(c.getColumnIndex(C_PRODUCT_QUANTITY));
                all.product_icon = c.getString(c.getColumnIndex(C_PRODUCT_ICON));
                all.product_types = c.getString(c.getColumnIndex(C_PRODUCT_TYPES));
                all.total_price = c.getDouble(c.getColumnIndex(C_TOTAL_PRICE));
                all.subtotal = c.getDouble(c.getColumnIndex(C_SUBTOTAL));

//                all.no_of_products = Integer.parseInt( c.getString( c.getColumnIndex( C_N0_OF_PRODUCTS ) ) );


                arr.add(all);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }


    public ArrayList<ModelCartArray> getJsonproduct() {

        ArrayList<ModelCartArray> arr = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_CART_ITEMS;

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {

                ModelCartArray all = new ModelCartArray();

                all.names = c.getString(c.getColumnIndex(C_PRODUCT_NAME));
                all.quantity = c.getString(c.getColumnIndex(C_PRODUCT_QUANTITY));
                all.images = c.getString(c.getColumnIndex(C_PRODUCT_ICON));
                all.varietyNames = c.getString(c.getColumnIndex(C_PRODUCT_TYPES));
                all.prices = c.getString(c.getColumnIndex(C_TOTAL_PRICE));
                all.id = c.getString(c.getColumnIndex(C_ID));
                all.product_id = c.getString(c.getColumnIndex(C_PRODUCT_ID));
                arr.add(all);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }

    public ArrayList<Cart_items_model> getcatproduct1(String product_types) {

        ArrayList<Cart_items_model> arr = new ArrayList<Cart_items_model>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_CART_ITEMS + " where product_types " + "='" + product_types + "'";

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {

                Cart_items_model all = new Cart_items_model();

                all.product_name = c.getString(c.getColumnIndex(C_PRODUCT_NAME));
                all.product_quantity = c.getString(c.getColumnIndex(C_PRODUCT_QUANTITY));
                all.product_icon = c.getString(c.getColumnIndex(C_PRODUCT_ICON));
                all.product_types = c.getString(c.getColumnIndex(C_PRODUCT_TYPES));
                all.total_price = c.getDouble(c.getColumnIndex(C_TOTAL_PRICE));
                all.subtotal = c.getDouble(c.getColumnIndex(C_SUBTOTAL));


                arr.add(all);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }


//**********************Update cart table **********************************************************

    public void updatecart(Cart_items_model model) {

        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            values.put(C_PRODUCT_QUANTITY, model.product_quantity);
            dataBaseHelper.getWritableDatabase().update(TABLE_CART_ITEMS, values, "id ='" + model.id + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void deletecart() {
        try {
            dataBaseHelper.createDataBase();

            dataBaseHelper.getWritableDatabase().delete(TABLE_CART_ITEMS, null, null);

            dataBaseHelper.close();
        } catch (Exception e) {
            //  e.printStackTrace();
        }
    }
//
//    public ArrayList<Model_view_allfolders> getfolderImage() {
//
//        ArrayList<Model_view_allfolders> arr = new ArrayList<Model_view_allfolders>();
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_ALLFOLDERS;
//
//            Cursor c = db.rawQuery(select, null);
//            while (c.moveToNext()) {
//
//                Model_view_allfolders all = new Model_view_allfolders();
//
//                all.id = c.getInt(c.getColumnIndex(ID));
//                all.folder_name = c.getString(c.getColumnIndex(FOLDER_NAME));
//                all.file_name = c.getString(c.getColumnIndex(FILE_NAME));
//                all.folder_path = c.getString(c.getColumnIndex(FOLDER_PATH));
//                all.no_of_pages = c.getString(c.getColumnIndex(NO_OF_PAGES));
//                all.folder_link = c.getString(c.getColumnIndex(FOLDER_LINK));
//                all.category_id = c.getString(c.getColumnIndex(CATEGORY_ID));
//                all.brand_id = c.getString(c.getColumnIndex(BRAND_ID));
//                all.location = c.getString(c.getColumnIndex(LOCATION));
//                all.lat = c.getString(c.getColumnIndex(LAT));
//                all.lng = c.getString(c.getColumnIndex(LNG));
//                all.search_keyword = c.getString(c.getColumnIndex(SEARCHKEYWORD));
//                all.background_color = c.getString(c.getColumnIndex(BACKGROUND_COLOR));
//                all.status = c.getString(c.getColumnIndex(STATUS));
//                all.no_of_clicks = c.getInt(c.getColumnIndex(NO_OF_CLICKS));
//                all.created_date = c.getString(c.getColumnIndex(CREATED_DATE));
//                all.expiry_date = c.getString(c.getColumnIndex(EXPIRY_DATE_A));
//                all.modified_date = c.getString(c.getColumnIndex(MODIFIED_DATE));
//                all.image0 = c.getString(c.getColumnIndex(IMAGE0));
//                all.fav = c.getString(c.getColumnIndex(FAV));
//
//
//                arr.add(all);
//
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return arr;
//    }
//
//    //*********************************GETTING DATA FROM TABLE_VIEW_FOLDER******************************************
//    public ArrayList<Model_View_folder> getviewfolder() {
//
//        ArrayList<Model_View_folder> arr = new ArrayList<Model_View_folder>();
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_VIEW_FOLDER;
//
//            Cursor c = db.rawQuery(select, null);
//            while (c.moveToNext()) {
//
//                Model_View_folder all = new Model_View_folder();
//
//                all.id = c.getInt(c.getColumnIndex(ID_V));
//                all.folder_name = c.getString(c.getColumnIndex(FOLDER_NAME_V));
//                all.file_name = c.getString(c.getColumnIndex(FILE_NAME_V));
//                all.folder_path = c.getString(c.getColumnIndex(FOLDER_PATH_V));
//                all.no_of_pages = c.getString(c.getColumnIndex(NO_OF_PAGES_V));
//                all.category_id = c.getString(c.getColumnIndex(CATEGORY_ID_V));
//                all.brand_id = c.getString(c.getColumnIndex(BRAND_ID_V));
//                all.location = c.getString(c.getColumnIndex(LOCATION_V));
//                all.lat = c.getString(c.getColumnIndex(LAT_V));
//                all.lng = c.getString(c.getColumnIndex(LNG_V));
//                ;
//                all.status = c.getString(c.getColumnIndex(STATUS_V));
//                all.no_of_clicks = c.getString(c.getColumnIndex(NO_OF_CLICKS_V));
//                all.created_date = c.getString(c.getColumnIndex(CREATED_DATE_V));
//                all.modified_date = c.getString(c.getColumnIndex(MODIFIED_DATE_V));
//                all.expiry_date = c.getString(c.getColumnIndex(EXPIRY_DATE));
//                all.image = c.getString(c.getColumnIndex(IMAGE));
//
//
//                arr.add(all);
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return arr;
//    }
//
//    //*********************************GETTING DATA FROM TABLE_VIEW_FOLDER******************************************
//    public ArrayList<Model_View_folder> getviewfolder2(int id) {
//
//        ArrayList<Model_View_folder> arr = new ArrayList<Model_View_folder>();
//        Cursor c = null;
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_VIEW_FOLDER + " WHERE id='" + id + "'";
//
//            c = db.rawQuery(select, null);
//            while (c.moveToNext()) {
//
//                Model_View_folder all = new Model_View_folder();
//
//                all.id = c.getInt(c.getColumnIndex(ID_V));
//                all.folder_name = c.getString(c.getColumnIndex(FOLDER_NAME_V));
//                all.file_name = c.getString(c.getColumnIndex(FILE_NAME_V));
//                all.folder_path = c.getString(c.getColumnIndex(FOLDER_PATH_V));
//                all.no_of_pages = c.getString(c.getColumnIndex(NO_OF_PAGES_V));
//                all.category_id = c.getString(c.getColumnIndex(CATEGORY_ID_V));
//                all.brand_id = c.getString(c.getColumnIndex(BRAND_ID_V));
//                all.location = c.getString(c.getColumnIndex(LOCATION_V));
//                all.lat = c.getString(c.getColumnIndex(LAT_V));
//                all.lng = c.getString(c.getColumnIndex(LNG_V));
//                ;
//                all.status = c.getString(c.getColumnIndex(STATUS_V));
//                all.no_of_clicks = c.getString(c.getColumnIndex(NO_OF_CLICKS_V));
//                all.created_date = c.getString(c.getColumnIndex(CREATED_DATE_V));
//                all.modified_date = c.getString(c.getColumnIndex(MODIFIED_DATE_V));
//                all.expiry_date = c.getString(c.getColumnIndex(EXPIRY_DATE));
//                all.image = c.getString(c.getColumnIndex(IMAGE));
//
//
//                arr.add(all);
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return arr;
//    }
//
//    //*********************************GETTING DATA FROM TABLE_ALLFOLDERS******************************************
//    public ArrayList<Model_view_allfolders> getfav2(int[] ids, int position) {
//        String select = null;
//        Cursor c = null;
//        ArrayList<Model_view_allfolders> arr = new ArrayList<Model_view_allfolders>();
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            //  for (int i = 0; i < ids.length; i++) {
//            if (Dashboard.posslider == 2) {
//                select = "SELECT * FROM " + TABLE_ALLFOLDERS + " ORDER BY " + NO_OF_CLICKS + " DESC";
//            }
//
//            c = db.rawQuery(select, null);
//            while (c.moveToNext()) {
//
//                Model_view_allfolders all = new Model_view_allfolders();
//
//                all.id = c.getInt(c.getColumnIndex(ID));
//                all.folder_name = c.getString(c.getColumnIndex(FOLDER_NAME));
//                all.file_name = c.getString(c.getColumnIndex(FILE_NAME));
//                all.folder_path = c.getString(c.getColumnIndex(FOLDER_PATH));
//                all.no_of_pages = c.getString(c.getColumnIndex(NO_OF_PAGES));
//                all.expiry_date = c.getString(c.getColumnIndex(EXPIRY_DATE_A));
//                all.background_color = c.getString(c.getColumnIndex(BACKGROUND_COLOR));
//
//                all.category_id = c.getString(c.getColumnIndex(CATEGORY_ID));
//                all.brand_id = c.getString(c.getColumnIndex(BRAND_ID));
//                all.location = c.getString(c.getColumnIndex(LOCATION));
//                all.lat = c.getString(c.getColumnIndex(LAT));
//                all.lng = c.getString(c.getColumnIndex(LNG));
//                ;
//                all.status = c.getString(c.getColumnIndex(STATUS));
//                all.no_of_clicks = c.getInt(c.getColumnIndex(NO_OF_CLICKS));
//                all.created_date = c.getString(c.getColumnIndex(CREATED_DATE));
//                all.modified_date = c.getString(c.getColumnIndex(MODIFIED_DATE));
//                all.image0 = c.getString(c.getColumnIndex(IMAGE0));
//
//
//                arr.add(all);
//                // }
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (
//                Exception e
//                )
//
//        {
//            //  e.printStackTrace();
//        }
//
//        return arr;
//    }
//
//    //*********************************GETTING DATA FROM TABLE_ALLFOLDERS******************************************
//    public ArrayList<Model_view_allfolders> getfavt() {
//        String select = null;
//        Cursor c = null;
//        ArrayList<Model_view_allfolders> arr = new ArrayList<Model_view_allfolders>();
//        int size = arr.size();
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            if (Dashboard.posslider == 3) {
//                select = "SELECT * FROM " + TABLE_ALLFOLDERS+ " WHERE fav='" + "1" + "'";
//            } else {
//                select = "SELECT * FROM " + TABLE_ALLFOLDERS + " WHERE fav='" + "1" + "'";
//            }
//
//
//            c = db.rawQuery(select, null);
//            while (c.moveToNext()) {
//
//                Model_view_allfolders all = new Model_view_allfolders();
//
//                all.id = c.getInt(c.getColumnIndex(ID));
//                all.folder_name = c.getString(c.getColumnIndex(FOLDER_NAME));
//                all.file_name = c.getString(c.getColumnIndex(FILE_NAME));
//                all.folder_path = c.getString(c.getColumnIndex(FOLDER_PATH));
//                all.no_of_pages = c.getString(c.getColumnIndex(NO_OF_PAGES));
//
//                all.category_id = c.getString(c.getColumnIndex(CATEGORY_ID));
//                all.brand_id = c.getString(c.getColumnIndex(BRAND_ID));
//                all.location = c.getString(c.getColumnIndex(LOCATION));
//                all.lat = c.getString(c.getColumnIndex(LAT));
//                all.lng = c.getString(c.getColumnIndex(LNG));
//                ;
//                all.status = c.getString(c.getColumnIndex(STATUS));
//                all.no_of_clicks = c.getInt(c.getColumnIndex(NO_OF_CLICKS));
//                all.created_date = c.getString(c.getColumnIndex(CREATED_DATE));
//                all.modified_date = c.getString(c.getColumnIndex(MODIFIED_DATE));
//                all.image0 = c.getString(c.getColumnIndex(IMAGE0));
//                all.fav = c.getString(c.getColumnIndex(FAV));
//
//
//                arr.add(all);
//            }
//
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (
//                Exception e
//                )
//
//        {
//            //  e.printStackTrace();
//        }
//
//        return arr;
//    }
//
//    //*********************************GETTING DATA FROM TABLE_ALLFOLDERS on the basis of Fav id ******************************************
//    public ArrayList<Model_view_allfolders> getfavts22(ArrayList<Model_Favourite> model) {
//        String select = null;
//        Cursor c = null;
//        ArrayList<Model_view_allfolders> arr = new ArrayList<Model_view_allfolders>();
//        int size = arr.size();
//        for (int i = 0; i < model.size(); i++) {
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//
//                System.out.println("SELECT:::"+ model.get(i).id);
//                select = "SELECT * FROM " + TABLE_ALLFOLDERS + " WHERE id='" + model.get(i).id + "'";
//
//
//                c = db.rawQuery(select, null);
//                while (c.moveToNext()) {
//
//                    Model_view_allfolders all = new Model_view_allfolders();
//
//                    all.id = c.getInt(c.getColumnIndex(ID));
//                    all.folder_name = c.getString(c.getColumnIndex(FOLDER_NAME));
//                    all.file_name = c.getString(c.getColumnIndex(FILE_NAME));
//                    all.folder_path = c.getString(c.getColumnIndex(FOLDER_PATH));
//                    all.no_of_pages = c.getString(c.getColumnIndex(NO_OF_PAGES));
//
//                    all.category_id = c.getString(c.getColumnIndex(CATEGORY_ID));
//                    all.brand_id = c.getString(c.getColumnIndex(BRAND_ID));
//                    all.location = c.getString(c.getColumnIndex(LOCATION));
//                    all.lat = c.getString(c.getColumnIndex(LAT));
//                    all.lng = c.getString(c.getColumnIndex(LNG));
//                    all.expiry_date = c.getString(c.getColumnIndex(EXPIRY_DATE_A));
//                    ;
//                    all.status = c.getString(c.getColumnIndex(STATUS));
//                    all.no_of_clicks = c.getInt(c.getColumnIndex(NO_OF_CLICKS));
//                    all.created_date = c.getString(c.getColumnIndex(CREATED_DATE));
//                    all.modified_date = c.getString(c.getColumnIndex(MODIFIED_DATE));
//                    all.image0 = c.getString(c.getColumnIndex(IMAGE0));
//                    all.fav = c.getString(c.getColumnIndex(FAV));
//
//
//                    arr.add(all);
//                }
//
//
//                dataBaseHelper.close();
//                db.close();
//                c.close();
//
//            }catch(
//                    Exception e
//            )
//            {
//                //  e.printStackTrace();
//            }
//        }
//
//            return arr;
//        }
//
//        //*********************************GETTING DATA FROM TABLE_ALLFOLDERS******************************************
//        public ArrayList<Model_Favourite> getfavstatus () {
//            String select = null;
//            Cursor c = null;
//            ArrayList<Model_Favourite> arr = new ArrayList<Model_Favourite>();
//            int size = arr.size();
//            try {
//                dataBaseHelper.createDataBase();
//
//                SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
////for(int u=0;)
//                select = "SELECT * FROM " + TABLE_FAVOURITE ;//+ " WHERE id='" +id+ "'";
//
//
//                c = db.rawQuery(select, null);
//                while (c.moveToNext()) {
//
//                    Model_Favourite all = new Model_Favourite();
//
//                    all.id = c.getInt(c.getColumnIndex(FAV_ID));
//                    all.fav = c.getString(c.getColumnIndex(FAV_STATUS));
//
//
//                    arr.add(all);
//                }
//
//
//                dataBaseHelper.close();
//                db.close();
//                c.close();
//
//            } catch (
//                    Exception e
//                    )
//
//            {
//                //  e.printStackTrace();
//            }
//
//            return arr;
//        }
//        //*********************************GETTING DATA FROM TABLE_ALLFOLDERS******************************************
//        public ArrayList<Model_view_allfolders> getfavt3 () {
//            String select = null;
//            Cursor c = null;
//            ArrayList<Model_view_allfolders> arr = new ArrayList<Model_view_allfolders>();
//            int size = arr.size();
//            try {
//                dataBaseHelper.createDataBase();
//
//                SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//                if (Dashboard.posslider == 3) {
//                    select = "SELECT * FROM " + TABLE_ALLFOLDERS + " WHERE fav='" + "1" + "'";
//                } else {
//                    select = "SELECT * FROM " + TABLE_ALLFOLDERS + " WHERE fav='" + "1" + "'";
//                }
//
//
//                c = db.rawQuery(select, null);
//                while (c.moveToNext()) {
//
//                    Model_view_allfolders all = new Model_view_allfolders();
//
//                    all.id = c.getInt(c.getColumnIndex(ID));
//                    all.folder_name = c.getString(c.getColumnIndex(FOLDER_NAME));
//                    all.file_name = c.getString(c.getColumnIndex(FILE_NAME));
//                    all.folder_path = c.getString(c.getColumnIndex(FOLDER_PATH));
//                    all.no_of_pages = c.getString(c.getColumnIndex(NO_OF_PAGES));
//
//                    all.category_id = c.getString(c.getColumnIndex(CATEGORY_ID));
//                    all.brand_id = c.getString(c.getColumnIndex(BRAND_ID));
//                    all.location = c.getString(c.getColumnIndex(LOCATION));
//                    all.lat = c.getString(c.getColumnIndex(LAT));
//                    all.lng = c.getString(c.getColumnIndex(LNG));
//                    ;
//                    all.status = c.getString(c.getColumnIndex(STATUS));
//                    all.no_of_clicks = c.getInt(c.getColumnIndex(NO_OF_CLICKS));
//                    all.created_date = c.getString(c.getColumnIndex(CREATED_DATE));
//                    all.modified_date = c.getString(c.getColumnIndex(MODIFIED_DATE));
//                    all.image0 = c.getString(c.getColumnIndex(IMAGE0));
//                    all.fav = c.getString(c.getColumnIndex(FAV));
//
//
//                    arr.add(all);
//                }
//
//
//                dataBaseHelper.close();
//                db.close();
//                c.close();
//
//            } catch (
//                    Exception e
//                    )
//
//            {
//                //  e.printStackTrace();
//            }
//
//            return arr;
//        }
//
//        //*********************************GETTING DATA FROM TABLE_ALLFOLDERS******************************************
//        public ArrayList<Model_view_allfolders> getfav (ArrayList < Integer > ids,int position){
//            String select = null;
//            Cursor c = null;
//            ArrayList<Model_view_allfolders> arr = new ArrayList<Model_view_allfolders>();
//            try {
//                dataBaseHelper.createDataBase();
//
//                SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//                for (int i = 0; i < ids.size(); i++) {
//                    if (Dashboard.posslider == position) {
//                        select = "SELECT * FROM " + TABLE_ALLFOLDERS + " WHERE category_id LIKE'" + "%" + ids.get(i) + "%" + "'";
//
//                    }
////                else if(Dashboard.posslider==2){
////                    select = "SELECT * FROM " + TABLE_ALLFOLDERS + " ORDER BY " + NO_OF_CLICKS + " DESC";
////                }
//
//
//                    c = db.rawQuery(select, null);
//                    while (c.moveToNext()) {
//
//                        Model_view_allfolders all = new Model_view_allfolders();
//
//                        all.id = c.getInt(c.getColumnIndex(ID));
//                        all.folder_name = c.getString(c.getColumnIndex(FOLDER_NAME));
//                        all.file_name = c.getString(c.getColumnIndex(FILE_NAME));
//                        all.folder_path = c.getString(c.getColumnIndex(FOLDER_PATH));
//                        all.no_of_pages = c.getString(c.getColumnIndex(NO_OF_PAGES));
//
//                        all.category_id = c.getString(c.getColumnIndex(CATEGORY_ID));
//                        all.brand_id = c.getString(c.getColumnIndex(BRAND_ID));
//                        all.location = c.getString(c.getColumnIndex(LOCATION));
//                        all.lat = c.getString(c.getColumnIndex(LAT));
//                        all.lng = c.getString(c.getColumnIndex(LNG));
//                        all.expiry_date = c.getString(c.getColumnIndex(EXPIRY_DATE_A));
//                        ;
//                        all.status = c.getString(c.getColumnIndex(STATUS));
//                        all.no_of_clicks = c.getInt(c.getColumnIndex(NO_OF_CLICKS));
//                        all.created_date = c.getString(c.getColumnIndex(CREATED_DATE));
//                        all.modified_date = c.getString(c.getColumnIndex(MODIFIED_DATE));
//                        all.image0 = c.getString(c.getColumnIndex(IMAGE0));
//
//
//                        arr.add(all);
//                    }
//                }
//
//                dataBaseHelper.close();
//                db.close();
//                c.close();
//
//            } catch (
//                    Exception e
//                    )
//
//            {
//                //  e.printStackTrace();
//            }
//
//            return arr;
//        }
//
//
//        //**********************INSERT into getcrop Table*****************
//
//    public void getcrop(Model_cropImage model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populate_crop(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_CROPIMAGE, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//
//    //************************************GetcropImages****************************************
//
//    public ArrayList<Model_cropImage> GetcropImages() throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//            e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//
//        String select = "SELECT * FROM " + TABLE_CROPIMAGE;
//
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<Model_cropImage> array = new ArrayList<>();
//        while (c.moveToNext()) {
//
//            Model_cropImage img = new Model_cropImage();
//            img.cropimage = c.getString(c.getColumnIndex("cropimage"));
////            img.cropimage=populate_crop(img);
//            array.add((img));
//            //array.add(img);
//        }
//
//
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return array;
//
//
//    }
//
//    public boolean favoriteDelete(int id) {
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        return db.delete(TABLE_FAVOURITE, FAV_ID +  "=" + id, null) > 0;
//    }
////*****************delete row from TABLE_CROPIMAGE *******************************
//
//    public void deleteid(int id) {
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        db.delete(TABLE_FAVOURITE, FAV_ID + " = '" + id + "'", null);
//    }
//
//
//    //*****************delete row from TABLE_CROPIMAGE *******************************
//
//    public void deleteEntry(String key_name) {
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        db.delete(TABLE_CROPIMAGE, CROPIMAGE + " = '" + key_name + "'", null);
//    }
//
//    //*******************Delete data on reset*******************
//    public void deleteallfolders(String table) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //  e.printStackTrace();
//        }
//        dataBaseHelper.getWritableDatabase().delete(table, null, null);
//        try {
//            dataBaseHelper.close();
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//    }
//
//    public void updatefav(Model_view_allfolders model) {
////Model_view_allfolders model= new Model_view_allfolders();
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//
//        values.put(FAV, model.fav);
//
//        dataBaseHelper.getWritableDatabase().update(TABLE_ALLFOLDERS, values, "id ='" + model.id + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    //*********************************GETTING DATA FROM TABLE_ALLFOLDERS******************************************
//    public ArrayList<Model_view_allfolders> getfav3(ArrayList<String> ids, int position) {
//        String select = null;
//        Cursor c = null;
//        ArrayList<Model_view_allfolders> arr = new ArrayList<Model_view_allfolders>();
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//            for (int i = 0; i < ids.size(); i++) {
//
//                select = "SELECT * FROM " + TABLE_ALLFOLDERS + " WHERE category_id LIKE'" + "%" + ids.get(i) + "%" + "'";
//
////                else if(Dashboard.posslider==2){
////                    select = "SELECT * FROM " + TABLE_ALLFOLDERS + " ORDER BY " + NO_OF_CLICKS + " DESC";
////                }
//
//
//                c = db.rawQuery(select, null);
//                while (c.moveToNext()) {
//
//                    Model_view_allfolders all = new Model_view_allfolders();
//
//                    all.id = c.getInt(c.getColumnIndex(ID));
//                    all.folder_name = c.getString(c.getColumnIndex(FOLDER_NAME));
//                    all.file_name = c.getString(c.getColumnIndex(FILE_NAME));
//                    all.folder_path = c.getString(c.getColumnIndex(FOLDER_PATH));
//                    all.no_of_pages = c.getString(c.getColumnIndex(NO_OF_PAGES));
//                    all.background_color = c.getString(c.getColumnIndex(BACKGROUND_COLOR));
//
//                    all.category_id = c.getString(c.getColumnIndex(CATEGORY_ID));
//                    all.brand_id = c.getString(c.getColumnIndex(BRAND_ID));
//                    all.location = c.getString(c.getColumnIndex(LOCATION));
//                    all.lat = c.getString(c.getColumnIndex(LAT));
//                    all.lng = c.getString(c.getColumnIndex(LNG));
//                    all.expiry_date = c.getString(c.getColumnIndex(EXPIRY_DATE_A));
//                    ;
//                    all.status = c.getString(c.getColumnIndex(STATUS));
//                    all.no_of_clicks = c.getInt(c.getColumnIndex(NO_OF_CLICKS));
//                    all.created_date = c.getString(c.getColumnIndex(CREATED_DATE));
//                    all.modified_date = c.getString(c.getColumnIndex(MODIFIED_DATE));
//                    all.image0 = c.getString(c.getColumnIndex(IMAGE0));
//                    if (position == all.id) {
//
//                    } else {
//                        if (arr.size() < 6) {
//                            arr.add(all);
//                        }
//                    }
//                }
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (
//                Exception e
//                )
//
//        {
//            e.printStackTrace();
//        }
//
//        return arr;
//    }
//
//    public boolean existfolder(int id) {
//
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_VIEW_FOLDER + " WHERE id='" + id + "'";
//
//            Cursor c = db.rawQuery(select, null);
//            while (c.moveToNext()) {
//                return true;
//
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return false;
//    }


    //**********************INSERT into add_detail_customer Table*****************
//
//    public void add_detail_customer(Model_Add_Detail_Customer model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populateAdd_detail_Customer(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_ADD_DETAIL_CUSTOMER, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    public void update_detail_customer(Model_Add_Detail_Customer model) {
//
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//        values.put(WS_USERS_ID, model.ws_users_id);
//        values.put(FIRST_NAME, model.first_name);
//        values.put(LAST_NAME, model.last_name);
//        values.put(ADD_1, model.add_1);
//        values.put(ADD_2, model.add_2);
//        values.put(ADD_3, model.add_3);
//        values.put(SUBURB, model.suburb);
//        values.put(POSTCODE, model.postcode);
//        values.put(TEL_1, model.tel_1);
//        values.put(TEL_2, model.tel_2);
//        values.put(FAX, model.fax);
//        values.put(EMAIL, model.email);
//        values.put(DEFAULT_SHIPPING_ADD, model.default_shipping_add);
//        values.put(DEFAULT_BILLING_ADD, model.default_billing_add);
//        values.put(STATUS, model.status);
//        values.put(CREATED_DATE, model.created_date);
//        values.put(UPDATE_DATE, model.update_date);
//        values.put(UPDATED_BY, model.updated_by);
//        values.put(COUNTRY, model.country);
//        values.put(STATE, model.state);
//        dataBaseHelper.getWritableDatabase().update(TABLE_ADD_DETAIL_CUSTOMER, values, "prefix_name ='" + model.prefix_name + "' AND id='" + model.id + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//    //**********************INSERT into Getcategories Table*****************
//
//    public void getcategories(Model_GetCategories model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populateGetCategoties(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_GETCATEGORIES, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    //**********************INSERT into GetPage Table*****************
//
//    public void getpage(Model_GetPage model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populateGetPage(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_GETPAGE, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    public void updatepage(Model_GetPage model) {
//
//        try {
//            dataBaseHelper.createDataBase();
//
//            ContentValues values = new ContentValues();
//            // values.put(PAGE_ID, model.id);
//            values.put(PAGE_URL, model.page_url);
//            values.put(PAGE_STATUS, model.status);
//            dataBaseHelper.getWritableDatabase().update(TABLE_GETPAGE, values, "prefix_name ='" + model.prefix_name + "' AND id='" + model.id + "'", null);
//
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    //**********************INSERT into GetProducts Table*****************
//
//    public void getproduct(Model_GetProduct model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populategetproduct(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_GET_PRODUCTS, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    //**********************INSERT into GetProducts Table*****************
//
//    public void getproductImage(Model_Product_Image model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populateGetProductImage(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_GET_PRODUCT_IMAGE, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    //**********************INSERT into GetProducts Inventory*****************
//
//
//    public void getproductInventory(Model_Product_Inventory model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populateGetProductInventory(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_GET_PRODUCT_INVENTORY, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//    //**********************INSERT into GetTier Pricing*****************
//
//    public void gettierpricing(Model_Tier_pricing model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populatetierpricing(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_TIER_PRICING, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//    //**********************INSERT into Order Info*****************
//
//    public void getorderinfo(Model_Order_Info model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populateorderinfo(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_ORDER_INFO, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//    //**********************INSERT into Order Product*****************
//
//    public void getorderproduct(Model_Order_Product model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populateOrderProduct(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_ORDER_PRODUCT, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    //**********************INSERT into Order TEMPLATE*****************
//
//    public void gettemplate(Model_Template model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populateOrderTemplate(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_ORDER_TEMPLATE, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    //**************************Get Get Categories***********************
//    public String Getlastsync_id(String table, String prefix) throws Exception {
//        String id = "0";
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + table + " WHERE prefix_name='" + prefix + "'";
//
//            Cursor c = db.rawQuery(select, null);
//
//
//            while (c.moveToNext()) {
//
//                id = c.getString(c.getColumnIndex("last_sync_id"));
//            }
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return id;
//    }
//
//    //*****************Get Values from GetCatagories************************************************
///*
//    public ArrayList<Model_GetCategories> Get_Product_Catagory(String prefix, String parent_cat_id) {
//
//        ArrayList<Model_GetCategories> arr = new ArrayList<Model_GetCategories>();
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_GETCATEGORIES + " WHERE " + PREFIX_GETCATEGORIES + " = '" + prefix + "'" + " AND " + PARENT_CAT_ID + " = '" + parent_cat_id + "'";
//
//            Cursor c = db.rawQuery(select, null);
//            while (c.moveToNext()) {
//
//                Model_GetCategories modelGetCategories = new Model_GetCategories();
//                modelGetCategories.id = c.getString(c.getColumnIndex("id"));
//                modelGetCategories.category_name = c.getString(c.getColumnIndex("category_name"));
//                modelGetCategories.parent_cat_id = c.getString(c.getColumnIndex("parent_cat_id"));
//                modelGetCategories.root_cat_id = c.getString(c.getColumnIndex("root_cat_id"));
//                modelGetCategories.level = c.getString(c.getColumnIndex("level"));
//                modelGetCategories.sort = c.getString(c.getColumnIndex("sort"));
//                modelGetCategories.status = c.getString(c.getColumnIndex("status"));
//                arr.add(modelGetCategories);
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return arr;
//    }*/
//    public ArrayList<Model_Cat_Prod> Get_Prod_Cat(String prefix, String parent_cat_id) {
//
//        ArrayList<Model_Cat_Prod> arr = new ArrayList<Model_Cat_Prod>();
//
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_GETCATEGORIES + " WHERE " + PREFIX_GETCATEGORIES + " = '" + prefix + "'" + " AND " + PARENT_CAT_ID + " = '" + parent_cat_id + "'";
//
//            Cursor c = db.rawQuery(select, null);
//            int cantt = c.getCount();
//            if (cantt > 0) {
//                Model_Cat_Prod pmodel = new Model_Cat_Prod();
//                pmodel.view_type = "CatHeader";
//                arr.add(pmodel);
//            }
//            while (c.moveToNext()) {
//
//                Model_Cat_Prod modelGetCategories = new Model_Cat_Prod();
//                modelGetCategories.only_cat_id = c.getString(c.getColumnIndex("id"));
//                modelGetCategories.category_name = c.getString(c.getColumnIndex("category_name"));
//                modelGetCategories.parent_cat_id = c.getString(c.getColumnIndex("parent_cat_id"));
//                modelGetCategories.root_cat_id = c.getString(c.getColumnIndex("root_cat_id"));
//                modelGetCategories.level = c.getString(c.getColumnIndex("level"));
//                modelGetCategories.sort = c.getString(c.getColumnIndex("sort"));
//                modelGetCategories.cat_status = c.getString(c.getColumnIndex("status"));
//                modelGetCategories.view_type = c.getString(c.getColumnIndex("view_type"));
//                arr.add(modelGetCategories);
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PRODUCT_CAT_ID + " = '" + parent_cat_id + "' AND prefix_name ='" + prefix + "'";
//
//            Cursor c = db.rawQuery(select, null);
//            int cntt = c.getCount();
//            if (cntt > 0) {
//                Model_Cat_Prod pmodel = new Model_Cat_Prod();
//                pmodel.view_type = "ProdHeader";
//                arr.add(pmodel);
//            }
//
//
//            while (c.moveToNext()) {
//
//                Model_Cat_Prod Product = new Model_Cat_Prod();
//                Product.prefix_name = c.getString(c.getColumnIndex("prefix_name"));
//                Product.order_unit_name = c.getString(c.getColumnIndex("order_unit_name"));
//                Product.base_unit_name = c.getString(c.getColumnIndex("base_unit_name"));
//                Product.prod_id = c.getString(c.getColumnIndex("id"));
//                Product.sink_id = c.getString(c.getColumnIndex("sink_id"));
//                Product.cat_id = c.getString(c.getColumnIndex("cat_id"));
//                Product.prod_cat_name = getcatname(Product.cat_id, prefix);
//                Product.product_code = c.getString(c.getColumnIndex("product_code"));
//                Product.name = c.getString(c.getColumnIndex("name"));
//                Product.desc = c.getString(c.getColumnIndex("desc"));
//                Product.prod_status = c.getString(c.getColumnIndex("status"));
//                Product.cost_price = c.getString(c.getColumnIndex("cost_price"));
//                Product.min_order_qut = c.getString(c.getColumnIndex("min_order_qut"));
//                Product.max_order_qut = c.getString(c.getColumnIndex("max_order_qut"));
//                Product.taxable = c.getString(c.getColumnIndex("taxable"));
//                Product.base_unit_id = c.getString(c.getColumnIndex("base_unit_id"));
//                Product.unit_price = c.getString(c.getColumnIndex("unit_price"));
//                Product.order_unit_id = c.getString(c.getColumnIndex("order_unit_id"));
//                Product.view_type = c.getString(c.getColumnIndex("view_type"));
//                Product.img = getimage(Product.prod_id, prefix, "image");
//                //   Product.largeimg = getimage(Product.prod_id,prefix);
//
//                Product.inventory = c.getString(c.getColumnIndex("inventory"));
//                if (Product.inventory.equals("1")) {
//                    Product.avail_stock = getinvent(Product.prod_id, prefix);
//                } else {
//                    Product.avail_stock = "No Limits";
//                }
//
//                Product.spcl_price = c.getString(c.getColumnIndex("spcl_price"));
//                Product.warehouse_name = c.getString(c.getColumnIndex("warehouse_name"));
//                Product.warehouse_loc = c.getString(c.getColumnIndex("warehouse_loc"));
//                Product.stock_loc = c.getString(c.getColumnIndex("stock_loc"));
//                Product.prod_created_date = c.getString(c.getColumnIndex("created_date"));
//                Product.prod_update_date = c.getString(c.getColumnIndex("update_date"));
//                Product.prod_updated_by = c.getString(c.getColumnIndex("updated_by"));
//                Product.prod_type = c.getString(c.getColumnIndex("type"));
//                Product.tier_price = c.getString(c.getColumnIndex("tier_price"));
//                arr.add(Product);
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        return arr;
//    }
//    //*****************Get Values from Get_Product_id on the basis of ID************************************************
//
//    public ArrayList<Model_GetProduct> Get_Product_id(String prefix) {
//
//        ArrayList<Model_GetProduct> arr = new ArrayList<Model_GetProduct>();
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PREFIX_GET_PRODUCTS + " = '" + prefix + "'";
//
//            Cursor c = db.rawQuery(select, null);
//            while (c.moveToNext()) {
//
//                Model_GetProduct Product = new Model_GetProduct();
//                Product.prefix_name = c.getString(c.getColumnIndex("prefix_name"));
//                Product.order_unit_name = c.getString(c.getColumnIndex("order_unit_name"));
//                Product.base_unit_name = c.getString(c.getColumnIndex("base_unit_name"));
//                Product.id = c.getString(c.getColumnIndex("id"));
//                Product.sink_id = c.getString(c.getColumnIndex("sink_id"));
//                Product.cat_id = c.getString(c.getColumnIndex("cat_id"));
//                Product.cat_name = getcatname(Product.cat_id, prefix);
//                Product.product_code = c.getString(c.getColumnIndex("product_code"));
//                Product.name = c.getString(c.getColumnIndex("name"));
//                Product.desc = c.getString(c.getColumnIndex("desc"));
//                Product.status = c.getString(c.getColumnIndex("status"));
//                Product.cost_price = c.getString(c.getColumnIndex("cost_price"));
//                Product.min_order_qut = c.getString(c.getColumnIndex("min_order_qut"));
//                Product.max_order_qut = c.getString(c.getColumnIndex("max_order_qut"));
//                Product.taxable = c.getString(c.getColumnIndex("taxable"));
//                Product.base_unit_id = c.getString(c.getColumnIndex("base_unit_id"));
//                Product.unit_price = c.getString(c.getColumnIndex("unit_price"));
//                Product.order_unit_id = c.getString(c.getColumnIndex("order_unit_id"));
//                Product.view_type = c.getString(c.getColumnIndex("view_type"));
//                Product.img = getimage(Product.id, prefix, "image");
//
//                Product.inventory = c.getString(c.getColumnIndex("inventory"));
//                if (Product.inventory.equals("1")) {
//                    Product.avail_stock = getinvent(Product.id, prefix);
//                } else {
//                    Product.avail_stock = "No Limits";
//                }
//
//                Product.spcl_price = c.getString(c.getColumnIndex("spcl_price"));
//                Product.warehouse_name = c.getString(c.getColumnIndex("warehouse_name"));
//                Product.warehouse_loc = c.getString(c.getColumnIndex("warehouse_loc"));
//                Product.stock_loc = c.getString(c.getColumnIndex("stock_loc"));
//                Product.created_date = c.getString(c.getColumnIndex("created_date"));
//                Product.update_date = c.getString(c.getColumnIndex("update_date"));
//                Product.updated_by = c.getString(c.getColumnIndex("updated_by"));
//                Product.type = c.getString(c.getColumnIndex("type"));
//                Product.tier_price = c.getString(c.getColumnIndex("tier_price"));
//                arr.add(Product);
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return arr;
//    }
//
//
//    //**************************Get Get values***********************
//    public Model_Add_Detail_Customer Getvalues(String table, String prefix) throws Exception {
//        String fax, phone;
//        Model_Add_Detail_Customer customer = new Model_Add_Detail_Customer();
//        try {
//            dataBaseHelper.createDataBase();
//
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + table + " WHERE prefix_name='" + prefix + "'";
//
//            Cursor c = db.rawQuery(select, null);
//
//
//            while (c.moveToNext()) {
//
//                customer.fax = c.getString(c.getColumnIndex("fax"));
//                customer.tel_1 = c.getString(c.getColumnIndex("tel_1"));
//            }
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return customer;
//
//    }
//
//    //**************************Get GetTemplate_name***********************
//    public ArrayList<String> GetTemplate_name(String prefix) throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //  e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        String select = "SELECT * FROM " + TABLE_ORDER_TEMPLATE + " WHERE prefix_name='" + prefix + "'";
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<String> arr = new ArrayList<String>();
//
//        while (c.moveToNext()) {
//
//            arr.add(populateItem(c, "template_name"));
//
//        }
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return arr;
//    }
//
//    //**************************Get Get Image***********************
//    public ArrayList<Model_Order_Product> GetProductWimage(String table, String id, String prefix) throws Exception {
//        ArrayList<Model_Order_Product> arr = new ArrayList<Model_Order_Product>();
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + table + " WHERE " + PRODUCT_ORDER_ID + " = '" + id + "' AND prefix_name ='" + prefix + "'";
//
//            Cursor c = db.rawQuery(select, null);
//
//
//            while (c.moveToNext()) {
//                Model_Order_Product product = new Model_Order_Product();
//                product.order_id = id;
//                product.product_id = c.getString(c.getColumnIndex("product_id"));
//                product.product_name = c.getString(c.getColumnIndex("product_name"));
//                product.product_code = c.getString(c.getColumnIndex("product_code"));
//                product.image = getimage(product.product_id, prefix, "image");//caling getimage function
//                product.unit_price = c.getString(c.getColumnIndex("unit_price"));
//                product.qty = c.getString(c.getColumnIndex("qty"));
//                product.act_qty = c.getString(c.getColumnIndex("act_qty"));
//                product.sub_total = c.getString(c.getColumnIndex("sub_total"));
//                product.applied_price = c.getString(c.getColumnIndex("applied_price"));
//                product.taxable = getminmax(product.product_id, "taxable", prefix);
//                product.min_qty = getminmax(product.product_id, "min_order_qut", prefix);
//                product.max_qty = getminmax(product.product_id, "max_order_qut", prefix);
//                product.order_unit = getminmax(product.product_id, "order_unit_name", prefix);
//                product.base_unit = getminmax(product.product_id, "base_unit_name", prefix);
//                product.tier_price = getminmax(product.product_id, "tier_price", prefix);
//                product.spcl_price = getminmax(product.product_id, "spcl_price", prefix);
//                product.inventory = getminmax(product.product_id, "inventory", prefix);
//
//
//                arr.add(product);
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return arr;
//    }
//
//    private String getminmax(String product_id, String column, String prefix) {
//        String value = "";
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PRODUCT_ID + " = '" + product_id + "' AND prefix_name='" + prefix + "'";
//
//            Cursor c = db.rawQuery(select, null);
//
//
//            while (c.moveToNext()) {
//
//                value = c.getString(c.getColumnIndex(column));
//
//            }
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return value;
//    }
//
//    //***************Function for getting Image****
//    String getimage(String product_id, String prefix, String column) throws Exception {
//        //   String img = "https://appsglory.com.au/uploads/default-app.png";
//        String img = "default-app.png";
//
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_GET_PRODUCT_IMAGE + " WHERE " + IMAGE_PRODUCT_ID + " = '" + product_id + "' AND " + PREFIX_GET_PRODUCTS + "='" + prefix + "'";
//
//            Cursor c = db.rawQuery(select, null);
//
//
//            while (c.moveToNext()) {
//
//                img = c.getString(c.getColumnIndex(column));
//                String[] strArray = img.split("\\.");
//                if (strArray.length <= 1) {
//                    img = "default-app.png";
//                    System.out.println("default-app.png");
//                }
//
//
//            }
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return img;
//    }
//
//    //***************Function for getting Image****
//    String getcatname(String cat_id, String prefix) throws Exception {
//        String name = null;
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_GETCATEGORIES + " WHERE " + PREFIX_GETCATEGORIES + " = '" + prefix + "' AND id ='" + cat_id + "'";
//
//            Cursor c = db.rawQuery(select, null);
//
//
//            while (c.moveToNext()) {
//
//                name = c.getString(c.getColumnIndex("category_name"));
//
//            }
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return name;
//    }
//
//
//    //***************Function for getting Inventory****
//    public String getinvent(String product_id, String prefix) {
//        long avail = 0, reserve = 0;
//        String avail_stock = null;
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_GET_PRODUCT_INVENTORY + " WHERE " + INVENTORY_PRODUCT_ID + " = '" + product_id + "' AND prefix_name='" + prefix + "'";
//
//            Cursor c = db.rawQuery(select, null);
//
//            if (c != null) {
//                while (c.moveToNext()) {
//
//                    String availstock = c.getString(c.getColumnIndex("aval_stock"));
//                    availstock.replaceAll(" ", "");
//                    if (availstock != null || !availstock.equals("")
//                            || availstock.length() != 0) {
//                        try {
//                            avail = Long.parseLong(availstock);
//                        } catch (Exception e) {
//                            avail = 0;
//                        }
//                    }
//                    String resrvstock = c.getString(c.getColumnIndex("reserve_qut"));
//                    resrvstock.replaceAll(" ", "");
//                    if (resrvstock != null || !resrvstock.equals("")
//                            || resrvstock.length() != 0) {
//                        try {
//                            reserve = Long.parseLong(resrvstock);
//                        } catch (Exception e) {
//                            reserve = 0;
//                        }
//                    }
//                    long av = avail - reserve;
//                    if (av <= 0) {
//                        avail_stock = "0";
//                    } else {
//                        avail_stock = String.valueOf(av);
//                    }
//                }
//            } else {
//                avail_stock = "0";
//            }
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//            avail_stock = "0";
//        }
//        return avail_stock;
//
//    }
//
//    //    //**************************Get current Remedials Ground***********************
////    public ArrayList<String> GetRemedialCurrentGround(String item) throws Exception {
////        try {
////            dataBaseHelper.createDataBase();
////        } catch (IOException e) {
////
////
////            // e.printStackTrace();
////        }
////        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
////
////        String select = "SELECT * FROM " + TABLE_GROUND_REMEDIAL + " WHERE " + ITEM + " = '" + item + "'";
////
////        Cursor c = db.rawQuery(select, null);
////        ArrayList<String> arr = new ArrayList<String>();
////
////        while (c.moveToNext()) {
////
////            arr.add(populateItem(c, CURRENT_GROUND));
////
////        }
////        try {
////            dataBaseHelper.close();
////            db.close();
////            c.close();
////
////        } catch (Exception e) {
////            //   e.printStackTrace();
////        }
////        return arr;
////    }
////
//    //**************************Get Data from Template and add to Model_Templates ***********************
//    public ArrayList<Model_Template> GetTemplate(String prefix, String gettype) throws Exception {
//        String select = "";
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//
//            //  e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        if (gettype.equals("all")) {
//            select = "SELECT * FROM " + TABLE_ORDER_TEMPLATE + " WHERE " + PREFIX_ORDER_TEMPLATE + "='" + prefix + "'";
//        } else {
//            select = "SELECT * FROM " + TABLE_ORDER_TEMPLATE + " WHERE " + PREFIX_ORDER_TEMPLATE + "='" + prefix + "' AND " + TEMPLATE_TYPE + "='newupdate'";
//        }
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<Model_Template> arr = new ArrayList<Model_Template>();
//
//        while (c.moveToNext()) {
//
//            Model_Template item = new Model_Template();
//
//            item.order_template_id = populateItem(c, "order_template_id");
//            item.type = populateItem(c, "type");
//            item.customer_id = populateItem(c, "customer_id");
//            item.template_name = populateItem(c, "template_name");
//            item.product_code = populateItem(c, "product_code");
//            item.product_id = populateItem(c, "product_id");
//            item.qty = populateItem(c, "qty");
//            arr.add(item);
//        }
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return arr;
//    }
//
//    //**************************Get Data from Template and add to Model_Templates ***********************
//    public ArrayList<Model_GetProduct> GetTemplate_product(JSONArray qtyarray, JSONArray productarray, String prefix) {
//        ArrayList<Model_GetProduct> arr = new ArrayList<Model_GetProduct>();
//
//
//        try {
//            for (int i = 0; i < productarray.length(); i++) {
//                dataBaseHelper.createDataBase();
//                SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//                String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PRODUCT_ID + " = '" + productarray.get(i).toString() + "' AND " + PREFIX_NAME + " = '" + prefix + "'";
//                System.out.print("selct=" + select);
//                Cursor c = db.rawQuery(select, null);
//
//
//                while (c.moveToNext()) {
//
//
//                    Model_GetProduct item = new Model_GetProduct();
//                    item.id = populateItem(c, "id");
//                    item.unit_price = populateItem(c, "unit_price");
//                    item.inventory = populateItem(c, "inventory");
//                    item.min_order_qut = populateItem(c, "min_order_qut");
//                    item.max_order_qut = populateItem(c, "max_order_qut");
//                    item.order_unit_id = populateItem(c, "order_unit_id");
//                    item.base_unit_id = populateItem(c, "base_unit_id");
//                    item.order_unit_name = populateItem(c, "order_unit_name");
//                    item.base_unit_name = populateItem(c, "base_unit_name");
//                    item.spcl_price = populateItem(c, "spcl_price");
//                    item.taxable = populateItem(c, "taxable");
//                    item.product_code = populateItem(c, "product_code");
//                    item.name = populateItem(c, "name");
//                    item.qty = qtyarray.get(i).toString();
//                    item.img = getimage(productarray.get(i).toString(), prefix, "image");
//
//                    arr.add(item);
//                }
//
//
//                dataBaseHelper.close();
//                db.close();
//                c.close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        return arr;
//    }
//
//    //**************************Get Data from Login and add to Model_Login ***********************
//    public ArrayList<ModelLogin> GetLogin_details() throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//
//            //  e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        String select = "SELECT * FROM " + TABLE_LOGIN;
//
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<ModelLogin> arr = new ArrayList<ModelLogin>();
//
//        while (c.moveToNext()) {
//
//            ModelLogin item = new ModelLogin();
//            item.company_name = populateItem(c, "company_name");
//            item.prefix_name = populateItem(c, "prefix_name");
//
//            item.auth_code = populateItem(c, "auth_code");
//
//            item.company_logo = populateItem(c, "company_logo");
//            item.currency_code = populateItem(c, "currency_code");
//            item.currency_symbol = populateItem(c, "currency_symbol");
//            item.shipping_status = populateItem(c, "shipping_status");
//            item.shipping_cost = populateItem(c, "shipping_cost");
//            item.tax_label = populateItem(c, "tax_label");
//            item.message_count = populateItem(c, "message_count");
//            item.tax_percentage = populateItem(c, "tax_percentage");
//            item.customer_business = populateItem(c, "customer_business");
//            arr.add(item);
//        }
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return arr;
//    }
//
//    //**************************Get Data from Login and add to Model_Login FOR OTHER USER***********************
//    public ArrayList<ModelLogin> GetLogin(String prefix) throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//
//            //  e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        String select = "SELECT * FROM " + TABLE_LOGIN + " WHERE " + PREFIX_NAME + " = '" + prefix + "'";
//
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<ModelLogin> arr = new ArrayList<ModelLogin>();
//
//        while (c.moveToNext()) {
//
//            ModelLogin item = new ModelLogin();
//            item.prefix_name = populateItem(c, "prefix_name");
//
//            item.auth_code = populateItem(c, "auth_code");
//            item.company_name = populateItem(c, "company_name");
//            item.company_logo = populateItem(c, "company_logo");
//            item.currency_code = populateItem(c, "currency_code");
//            item.currency_symbol = populateItem(c, "currency_symbol");
//            item.shipping_status = populateItem(c, "shipping_status");
//            item.shipping_cost = populateItem(c, "shipping_cost");
//            item.tax_label = populateItem(c, "tax_label");
//            item.message_count = populateItem(c, "message_count");
//            item.tax_percentage = populateItem(c, "tax_percentage");
//            item.customer_business = populateItem(c, "customer_business");
//            arr.add(item);
//        }
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return arr;
//    }
//
////**********************submitted order info where order status is submitted*********
//
//    public ArrayList<Model_Order_Info> Select_submittedorder_id(String prefix) throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//            e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//
//        String select = "SELECT * FROM " + TABLE_ORDER_INFO + " WHERE " + ORDER_STATUS + " = 'Submitted' AND prefix_name='" + prefix + "'";
//
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<Model_Order_Info> array = new ArrayList<Model_Order_Info>();
//        while (c.moveToNext()) {
//            Model_Order_Info info = new Model_Order_Info();
//            info.total = populateItem(c, ORDER_TOTAL);
//            info.order_id = populateItem(c, ORDER_ID);
//            info.order_by = populateItem(c, ORDER_BY);
//            info.order_name = populateItem(c, ORDER_NAME);
//            info.order_status = populateItem(c, ORDER_STATUS);
//            info.billing_suburb = populateItem(c, BILLING_SUBURB);
//            info.billing_add_1 = populateItem(c, BILLING_ADD_1);
//            info.billing_add_2 = populateItem(c, BILLING_ADD_2);
//            info.billing_add_3 = populateItem(c, BILLING_ADD_3);
//            info.billing_postcode = populateItem(c, BILLING_POSTCODE);
//            info.billing_country = populateItem(c, BILLING_COUNTRY);
//            info.billing_state = populateItem(c, BILLING_STATE);
//            info.shipping_country = populateItem(c, SHIPPING_COUNTRY);
//            info.shipping_state = populateItem(c, SHIPPING_STATE);
//            info.delivery_notes = populateItem(c, DELIVERY_NOTES);
//            info.order_notes = populateItem(c, ORDER_NOTES);
//            info.shipping_add_1 = populateItem(c, SHIPPING_ADD_1);
//            info.shipping_add_2 = populateItem(c, SHIPPING_ADD_2);
//            info.shipping_add_3 = populateItem(c, SHIPPING_ADD_3);
//            info.shipping_suburb = populateItem(c, SHIPPING_SUBURB);
//            info.shipping_postcode = populateItem(c, ORDER_SHIPPING_POSTCODE);
//            info.sub_total = populateItem(c, SUB_TOTAL);
//            info.tax_total = populateItem(c, ORDER_TAX_TOTAL);
//            info.shipping_cost = populateItem(c, SHIPPING_COST);
//            array.add(info);
//        }
//
//
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return array;
//
//
//    }
//    //**********************submitted order info where order status is Cancelled*********
//
//    public ArrayList<Model_Order_Info> Select_cancelorder_id(String prefix) throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//            e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//
//        String select = "SELECT * FROM " + TABLE_ORDER_INFO + " WHERE " + ORDER_STATUS + " = 'Cancelled' AND prefix_name='" + prefix + "'";
//
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<Model_Order_Info> array = new ArrayList<Model_Order_Info>();
//        while (c.moveToNext()) {
//            Model_Order_Info info = new Model_Order_Info();
//            info.total = populateItem(c, ORDER_TOTAL);
//            info.order_id = populateItem(c, ORDER_ID);
//            info.order_by = populateItem(c, ORDER_BY);
//            info.order_name = populateItem(c, ORDER_NAME);
//            info.order_status = populateItem(c, ORDER_STATUS);
//            info.billing_suburb = populateItem(c, BILLING_SUBURB);
//            info.billing_add_1 = populateItem(c, BILLING_ADD_1);
//            info.billing_add_2 = populateItem(c, BILLING_ADD_2);
//            info.billing_add_3 = populateItem(c, BILLING_ADD_3);
//            info.billing_postcode = populateItem(c, BILLING_POSTCODE);
//            info.billing_country = populateItem(c, BILLING_COUNTRY);
//            info.billing_state = populateItem(c, BILLING_STATE);
//            info.shipping_country = populateItem(c, SHIPPING_COUNTRY);
//            info.shipping_state = populateItem(c, SHIPPING_STATE);
//            info.delivery_notes = populateItem(c, DELIVERY_NOTES);
//            info.order_notes = populateItem(c, ORDER_NOTES);
//            info.shipping_add_1 = populateItem(c, SHIPPING_ADD_1);
//            info.shipping_add_2 = populateItem(c, SHIPPING_ADD_2);
//            info.shipping_add_3 = populateItem(c, SHIPPING_ADD_3);
//            info.shipping_suburb = populateItem(c, SHIPPING_SUBURB);
//            info.shipping_postcode = populateItem(c, ORDER_SHIPPING_POSTCODE);
//            info.sub_total = populateItem(c, SUB_TOTAL);
//            info.tax_total = populateItem(c, ORDER_TAX_TOTAL);
//            info.shipping_cost = populateItem(c, SHIPPING_COST);
//            array.add(info);
//        }
//
//
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return array;
//
//
//    }
//
//    //--------------------Get all address from add_detil_customer table----------------
//    public ArrayList<Model_Add_Detail_Customer> get_all_address(String prefix_name) throws Exception {
//        ArrayList<Model_Add_Detail_Customer> array = new ArrayList<Model_Add_Detail_Customer>();
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//
//            String select = "SELECT * FROM " + TABLE_ADD_DETAIL_CUSTOMER + " WHERE " + PREFIX_NAME + " = '" + prefix_name + "'";
//
//            Cursor c = db.rawQuery(select, null);
//
//            while (c.moveToNext()) {
//                Model_Add_Detail_Customer info = new Model_Add_Detail_Customer();
//                info.id = populateItem(c, ID);
//                info.ws_users_id = populateItem(c, WS_USERS_ID);
//                info.first_name = populateItem(c, FIRST_NAME);
//                info.last_name = populateItem(c, LAST_NAME);
//                info.add_1 = populateItem(c, ADD_1);
//                info.add_2 = populateItem(c, ADD_2);
//                info.add_3 = populateItem(c, ADD_3);
//                info.suburb = populateItem(c, SUBURB);
//                info.postcode = populateItem(c, POSTCODE);
//                info.tel_1 = populateItem(c, TEL_1);
//                info.tel_2 = populateItem(c, TEL_2);
//                info.fax = populateItem(c, FAX);
//                info.email = populateItem(c, EMAIL);
////            info.default_shipping_add = populateItem(c, DEFAULT_SHIPPING_ADD);
////            info.default_billing_add = populateItem(c, DEFAULT_BILLING_ADD);
////            info.status = populateItem(c, STATUS);
////            info.created_date = populateItem(c, CREATED_DATE);
////            info.update_date = populateItem(c, UPDATE_DATE);
////            info.updated_by = populateItem(c, UPDATED_BY);
//                info.country = populateItem(c, COUNTRY);
//                info.state = populateItem(c, STATE);
//                array.add(info);
//            }
//
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return array;
//
//
//    }
//
//    //--------------------Get shipping add_id and billing add_id from getcustomer table----------------
//    public ArrayList<ModelgetCustomer> getShippingAndBillingID(String prefix_name) throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//            e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//
//        String select = "SELECT * FROM " + TABLE_GETCUSTOMER + " WHERE " + PREFIX_NAME + " = '" + prefix_name + "'";
//
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<ModelgetCustomer> array = new ArrayList<ModelgetCustomer>();
//        while (c.moveToNext()) {
//
//            ModelgetCustomer info = new ModelgetCustomer();
//            info.billing_add_id = populateItem(c, BILLING_ADD_ID);
//            info.default_shipping_add_id = populateItem(c, DEFAULT_SHIPPING_ADD_ID);
//            array.add(info);
//        }
//
//
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return array;
//
//
//    }
//
//    //**********************INSERT into add_to_cart Table*****************
//    public void add_to_cart(Model_Add_To_Cart model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populateAddToCart(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_ADD_TO_CART, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//


//    //*****************Content Values for add_to_cart*************
//    private ContentValues populateAddToCart(Model_Add_To_Cart model) {
//
//        ContentValues values = new ContentValues();
//        values.put(PROD_ID, model.prod_id);
//        values.put(PROD_NAME, model.prod_name);
//        values.put(PROD_SKU, model.prod_sku);
//        values.put(CART_QTY, model.qty);
//        values.put(PRICE, model.price);
//        values.put(CART_SPCL_PRICE, model.spcl_price);
//        values.put(CART_MAX_QTY, model.max_qty);
//        values.put(CART_MIN_QTY, model.min_qty);
//        values.put(BASE_UNIT, model.base_unit);
//        values.put(ORDER_UNIT, model.order_unit);
//        values.put(CART_TAXABLE, model.taxable);
//        values.put(PROD_INVENT, model.inventory);
//
//        return values;
//    }
//
//    //**************************Get products added to CartFragment ***********************
//    public ArrayList<Model_Add_To_Cart> GetProductFromCart(String prefix) throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//
//            //  e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        String select = "SELECT * FROM " + TABLE_ADD_TO_CART;
//
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<Model_Add_To_Cart> arr = new ArrayList<Model_Add_To_Cart>();
//
//        while (c.moveToNext()) {
//
//            Model_Add_To_Cart item = new Model_Add_To_Cart();
//            item.prod_id = populateItem(c, "prod_id");
//            item.prod_name = populateItem(c, "prod_name");
//            item.prod_sku = populateItem(c, "prod_sku");
//            item.qty = populateItem(c, "qty");
//            item.price = populateItem(c, "price");
//            item.spcl_price = populateItem(c, "spcl_price");
//            item.max_qty = populateItem(c, "max_qty");
//            item.min_qty = populateItem(c, "min_qty");
//            item.base_unit = populateItem(c, "base_unit");
//            item.order_unit = populateItem(c, "order_unit");
//            item.taxable = populateItem(c, "taxable");
//            item.inventory = populateItem(c, "prod_inventory");
//            item.prod_image = getimage(item.prod_id, prefix, "image");
//            item.tier_price = gettierprice(item.prod_id, prefix);
//            arr.add(item);
//        }
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return arr;
//    }
//
//    private String gettierprice(String prod_id, String prefix) {
//
//        String tier = null;
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PRODUCT_ID + " = '" + prod_id + "' AND prefix_name='" + prefix + "'";
//
//            Cursor c = db.rawQuery(select, null);
//
//
//            while (c.moveToNext()) {
//
//                tier = c.getString(c.getColumnIndex("tier_price"));
//
//            }
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        System.out.print("tier=" + tier);
//        return tier;
//
//
//    }
//
//    //    **********************Get data from Order_info table***********************
//    public int getallcount(String prefix, String type) {
//        String countQuery = "";
//        if (type.equals("all")) {
//            countQuery = "SELECT  * FROM " + TABLE_ORDER_INFO + " WHERE " + PREFIX_ORDER_INFO + " = '" + prefix + "'";
//        } else {
//            countQuery = "SELECT  * FROM " + TABLE_ORDER_INFO + " WHERE " + PREFIX_ORDER_INFO + " = '" + prefix + "' AND " + ORDER_STATUS + " = '" + type + "'";
//
//        }
//        // String countQuery = "SELECT  * FROM " + TABLE_ORDER_INFO ;
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//        int cnt = cursor.getCount();
//        cursor.close();
//        return cnt;
//    }
//
//    //    **********************Get data from Order_info table***********************
//    public int getcartcount(String countQuery) {
//
//
//        // String countQuery = "SELECT  * FROM " + TABLE_ORDER_INFO ;
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//        int cnt = cursor.getCount();
//        cursor.close();
//        return cnt;
//    }
//
//
//    public ArrayList<Model_cropImage> Select_image(String select) throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//            // e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<Model_Order_Info> array = new ArrayList<Model_Order_Info>();
//        while (c.moveToNext()) {
//            Model_Order_Info info = new Model_Order_Info();
//            info.total = populateItem(c, ORDER_TOTAL);
//            info.created_date = populateItem(c, ORDER_CREATED_DATE);
//            info.order_id = populateItem(c, ORDER_ID);
//            info.order_by = populateItem(c, ORDER_BY);
//            info.order_name = populateItem(c, ORDER_NAME);
//            info.order_status = populateItem(c, ORDER_STATUS);
//            info.billing_suburb = populateItem(c, BILLING_SUBURB);
//            info.billing_add_1 = populateItem(c, BILLING_ADD_1);
//            info.billing_add_2 = populateItem(c, BILLING_ADD_2);
//            info.billing_add_3 = populateItem(c, BILLING_ADD_3);
//            info.billing_postcode = populateItem(c, BILLING_POSTCODE);
//            info.billing_country = populateItem(c, BILLING_COUNTRY);
//            info.billing_state = populateItem(c, BILLING_STATE);
//            info.shipping_country = populateItem(c, SHIPPING_COUNTRY);
//            info.shipping_state = populateItem(c, SHIPPING_STATE);
//            info.delivery_notes = populateItem(c, DELIVERY_NOTES);
//            info.order_notes = populateItem(c, ORDER_NOTES);
//            info.shipping_add_1 = populateItem(c, SHIPPING_ADD_1);
//            info.shipping_add_2 = populateItem(c, SHIPPING_ADD_2);
//            info.shipping_add_3 = populateItem(c, SHIPPING_ADD_3);
//            info.shipping_suburb = populateItem(c, SHIPPING_SUBURB);
//            info.shipping_postcode = populateItem(c, ORDER_SHIPPING_POSTCODE);
//            info.sub_total = populateItem(c, SUB_TOTAL);
//            info.tax_total = populateItem(c, ORDER_TAX_TOTAL);
//            info.shipping_cost = populateItem(c, SHIPPING_COST);
//            array.add(info);
//        }
//
//
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return array;
//
//
//    }


//    //*******************Delete all Ground Data*******************
//    public void deleteDataBase(String table) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //  e.printStackTrace();
//        }
//        dataBaseHelper.getWritableDatabase().delete(table, null, null);
//        try {
//            dataBaseHelper.close();
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//    }
//
//    //*******************Delete all tables*******************
//    public void delete(String table) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            // e.printStackTrace();
//        }
//        dataBaseHelper.getWritableDatabase().delete(table, null, null);
//        try {
//            dataBaseHelper.close();
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    public String Setvalues() {
//        String returnvalue = null;
//        boolean tba = false;
//        product_total = 0;
//        prod_tax = 0;
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//
//            //  e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        String select = "SELECT * FROM " + TABLE_ADD_TO_CART;
//
//        Cursor c = db.rawQuery(select, null);
//        while (c.moveToNext()) {
//            try {
//                double ptot = 0;
//                if (c.getString(c.getColumnIndex("prod_total")).equals("TBA")) {
//                    tba = true;
//                } else {
//                    ptot = Double.parseDouble(c.getString(c.getColumnIndex("prod_total")));
//                }
//                double ptax = Double.parseDouble(c.getString(c.getColumnIndex("prod_tax")));
//
//                product_total = product_total + ptot;
//                prod_tax = prod_tax + ptax;
//            } catch (Exception e) {
//            }
//        }
//        if (tba) {
//            returnvalue = "TBA";
//        } else {
//            returnvalue = String.valueOf(product_total) + ":" + String.valueOf(prod_tax);
//        }
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return returnvalue;
//    }
//
//    //-------------------update table_add_to_cart----------------
//    public void updatecartprodt(String prod_id, String prodtax, String prodtotalprice) {
//
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//        values.put(PROD_TAX, prodtax);
//        values.put(PROD_TOTAL, prodtotalprice);
//
//        dataBaseHelper.getWritableDatabase().update(TABLE_ADD_TO_CART, values, "prod_id ='" + prod_id + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//
//    }
//
//    //-------------------update table_add_to_cart----------------
//    public void updateaadid(String prefix, String shipingid, String billingid) {
//
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//        values.put(DEFAULT_SHIPPING_ADD, shipingid);
//        values.put(DEFAULT_BILLING_ADD, billingid);
//
//        dataBaseHelper.getWritableDatabase().update(TABLE_ADD_DETAIL_CUSTOMER, values, "prefix_name ='" + prefix + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//
//    }
//
//    public void updatecartqty(String prod_id, String qty) {
//
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//        ;
//        values.put(CART_QTY, qty);
//
//        dataBaseHelper.getWritableDatabase().update(TABLE_ADD_TO_CART, values, "prod_id ='" + prod_id + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//
//    }
//
//    //**********************INSERT into ADD_DETAIL_CUSTOMER Table*****************
///*    public void add_address(Model_Add_Detail_Customer model) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populateAddAdress(model);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_ADD_DETAIL_CUSTOMER, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }*/
//
//    //*****************Content Values for add_customer_address*************
//    private ContentValues populateAddAdress(Model_Add_Detail_Customer model) {
//        ContentValues values = new ContentValues();
//        values.put(PREFIX_ADD_DETAIL, model.prefix_name);
//        values.put(ID, model.id);
//        values.put(ADD_1, model.add_1);
//        values.put(COUNTRY, model.country);
//        values.put(STATE, model.state);
//        values.put(SUBURB, model.suburb);
//        values.put(POSTCODE, model.postcode);
//
//        return values;
//    }
//
//    public void update_prouct_Temp(String pcode, String prodid, String qty, String temp_order_id, String prefix_name, String type) {
//        try {
//            dataBaseHelper.createDataBase();
//
//            ContentValues values = new ContentValues();
//            values.put(TEMPLATE_QTY, qty);
//            values.put(TEMPLATE_PRODUCT_ID, prodid);
//            values.put(TEMPLATE_PRODUCT_CODE, pcode);
//            values.put(TEMPLATE_TYPE, type);
//
//            dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, "order_template_id ='" + temp_order_id + "' AND " + PREFIX_NAME + " = '" + prefix_name + "'", null);
//
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    //**************************Get Data from Template and add to Model_Templates ***********************
//    public ArrayList<Model_Template> GetTemp_Detail(String prefix, String tempid) throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//
//            //  e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        String select = "SELECT * FROM " + TABLE_ORDER_TEMPLATE + " WHERE " + ORDER_TEMPLATE_ID + " = '" + tempid + "' AND " + PREFIX_ORDER_TEMPLATE + " = '" + prefix + "'";
//
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<Model_Template> arr = new ArrayList<>();
//
//        while (c.moveToNext()) {
//
//            Model_Template item = new Model_Template();
//            item.order_template_id = populateItem(c, "order_template_id");
//            item.type = populateItem(c, "type");
//            item.customer_id = populateItem(c, "customer_id");
//            item.template_name = populateItem(c, "template_name");
//            item.product_code = populateItem(c, "product_code");
//            item.product_id = populateItem(c, "product_id");
//            item.qty = populateItem(c, "qty");
//            arr.add(item);
//        }
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return arr;
//    }
//    //*****************Get Values from get_notification************************************************
//
//    public ArrayList<Model_get_notification> get_notification_setting(String prefix) {
//
//        ArrayList<Model_get_notification> arr = new ArrayList<Model_get_notification>();
//        try {
//            dataBaseHelper.createDataBase();
//
//            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//            String select = "SELECT * FROM " + TABLE_NOTIFICATION_SETTINGS + " WHERE " + PREFIX_NAME_NOTI + " = '" + prefix + "'";
//
//            Cursor c = db.rawQuery(select, null);
//            while (c.moveToNext()) {
//
//                Model_get_notification modelGetCategories = new Model_get_notification();
//                modelGetCategories.push_notification = c.getString(c.getColumnIndex("push_notification"));
//                modelGetCategories.email_notification = c.getString(c.getColumnIndex("email_notification"));
//                modelGetCategories.time_zone = c.getString(c.getColumnIndex("timezone"));
//                arr.add(modelGetCategories);
//            }
//
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//        return arr;
//    }
//
//    //-------------------update notification Setting----------------
//    public void update_notification_setting(String email_notification_switch_value, String push_notification_switch_value
//            , String timeZOne_value, String prefix) {
//
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//        values.put(TIME_ZONE_NOTI, timeZOne_value);
//        values.put(PUSH_NOTIFICATION, push_notification_switch_value);
//        values.put(EMAIL_NOTIFICATION, email_notification_switch_value);
//
//
//        dataBaseHelper.getWritableDatabase().update(TABLE_NOTIFICATION_SETTINGS, values, "prefix_name ='" + prefix + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//
//    }
//
//    //-----------------------insert in  notification setting
//    public void add_get_notification(Model_get_notification item) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = populategetNotification(item);
//        dataBaseHelper.getWritableDatabase().insert(TABLE_NOTIFICATION_SETTINGS, null, values);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    public void update_get_notification(Model_get_notification model) {
//
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//
//        values.put(CURRENCY_CODE_NOTI, model.Currency_code);
//        values.put(CURRENCY_SYMBOL_NOTI, model.Currency_symbol);
//        values.put(TIME_ZONE_NOTI, model.time_zone);
//        values.put(PUSH_NOTIFICATION, model.push_notification);
//        values.put(EMAIL_NOTIFICATION, model.email_notification);
//        dataBaseHelper.getWritableDatabase().update(TABLE_NOTIFICATION_SETTINGS, values, "prefix_name ='" + model.Prefix_name + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//    }
//
//    //*****************Content Values for add_customer_address*************
//    private ContentValues populategetNotification(Model_get_notification model) {
//        ContentValues values = new ContentValues();
//        values.put(PREFIX_NAME_NOTI, model.Prefix_name);
//        values.put(CURRENCY_CODE_NOTI, model.Currency_code);
//        values.put(CURRENCY_SYMBOL_NOTI, model.Currency_symbol);
//        values.put(TIME_ZONE_NOTI, model.time_zone);
//        values.put(PUSH_NOTIFICATION, model.push_notification);
//        values.put(EMAIL_NOTIFICATION, model.email_notification);
//        return values;
//    }
//
//    //*******************Delete data on reset*******************
//    public void delete_table_data(String table, String column_name, String prefix) {
//        try {
//            dataBaseHelper.createDataBase();
//            dataBaseHelper.getWritableDatabase().delete(table, column_name + " = ?", new String[]{prefix});
//            dataBaseHelper.close();
//        } catch (IOException e) {
//
//        }
//    }
//
//    public void delete_temp(String name) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //  e.printStackTrace();
//        }
//        dataBaseHelper.getWritableDatabase().delete(TABLE_ORDER_TEMPLATE, ORDER_TEMPLATE_ID + " = ?", new String[]{name});
//        try {
//            dataBaseHelper.close();
//        } catch (Exception e) {
//            //  e.printStackTrace();
//        }
//    }
//
//    public void delete_value(String tablename, String column_name, String value) {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        dataBaseHelper.getWritableDatabase().delete(tablename, column_name + " = ?", new String[]{value});
//        try {
//            dataBaseHelper.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    //-------------------update table_Order Template----------------
//    public void update_temp_name(String order_id, String tempname, String prefix, String type) {
//
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//        values.put(TEMPLATE_NAME, tempname);
//        values.put(TEMPLATE_TYPE, type);
//
//
//        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, "order_template_id ='" + order_id + "' AND prefix_name ='" + prefix + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//
//    }
//
//    public void updateinventdb(String id, String product_id, String sink_id, String aval_stock, String reserve_qut, String bck_ord_aval, String instatus, String type, String lastsyncid, String prefix) {
//        try {
//            dataBaseHelper.createDataBase();
//
//            ContentValues values = new ContentValues();
//
//            values.put("sink_id", sink_id);
//            values.put("aval_stock", aval_stock);
//            values.put("reserve_qut", reserve_qut);
//            values.put("bck_ord_aval", bck_ord_aval);
//            values.put("status", instatus);
//            values.put("type", type);
//            values.put("last_sync_id", lastsyncid);
//
//            dataBaseHelper.getWritableDatabase().update(TABLE_GET_PRODUCT_INVENTORY, values, "product_id ='" + product_id + "' AND id ='" + id + "' AND prefix_name ='" + prefix + "'", null);
//
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//
//    }
//
//    //-------------------update table_Order Template----------------
//    public void update_temp_productId(String prodcode, String product_id, String qty, String template_id, String prefix) {
//
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//        values.put(TEMPLATE_PRODUCT_CODE, prodcode);
//        values.put(TEMPLATE_PRODUCT_ID, product_id);
//        values.put(TEMPLATE_QTY, qty);
//
//        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, "order_template_id ='" + template_id + "' AND prefix_name ='" + prefix + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//
//    }
//
//
//    //***********************************************************************
//    //-------------------update table_Order Template----------------
//    public void update_temp_qty(String pid, String qnty, String prefix, String type) {
//
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//
//        values.put(TEMPLATE_QTY, qnty);
//        values.put(TEMPLATE_TYPE, type);
//
//        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, "order_template_id ='" + pid + "' AND prefix_name ='" + prefix + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    //-------------------update table_Order Template----------------
//    public void update_newtemp(String order_id_new, String sink_id, String prefix, String type) {
//
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
//        ContentValues values = new ContentValues();
//
//        values.put(TEMPLATE_SINK_ID, sink_id);
//        values.put(TEMPLATE_TYPE, type);
//
//        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, ORDER_TEMPLATE_ID + " ='" + order_id_new + "' AND prefix_name ='" + prefix + "'", null);
//        try {
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            // e.printStackTrace();
//        }
//
//    }
//
//    //*********************update order_cancel_info**********************
//    public void update_cancel_info(String orderId, String prefix) {
//        try {
//            dataBaseHelper.createDataBase();
//
//            ContentValues values = new ContentValues();
//            values.put(ORDER_STATUS, "Cancelled");
//
//            dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_INFO, values, "order_id ='" + orderId + "' AND " + PREFIX_ORDER_INFO + " = '" + prefix + "'", null);
//
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    //-------------------------updateorderinfo------------------//
//    public void updateorderinfo(String prefix_name, String order_id, String shipping_cost, String total, String tax_total, String sub_total) {
//        try {
//            dataBaseHelper.createDataBase();
//
//            ContentValues values = new ContentValues();
//            values.put(ORDER_SHIPPING_COST, shipping_cost);
//            values.put(SUB_TOTAL, sub_total);
//            values.put(ORDER_TAX_TOTAL, tax_total);
//            values.put(ORDER_TOTAL, total);
//
//            dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_INFO, values, ORDER_ID + " = '" + order_id + "' AND " + PREFIX_ORDER_INFO + " = '" + prefix_name + "'", null);
//
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    //-----------------------updating order adress info-----//
//    public void updata_orderadd_info(String order_id, String prefix_name, String billing_country, String billing_state, String billing_suburb
//            , String billing_add1, String billing_add2, String billing_add3, String billing_postcode, String shipping_country, String shipping_state, String shipping_suburb
//            , String shipping_add1, String shipping_add2, String shipping_add3, String shipping_postcode, String text_order_notes, String text_order_by, String text_delivery_notes) {
//        try {
//            dataBaseHelper.createDataBase();
//
//            ContentValues values = new ContentValues();
//            values.put(BILLING_COUNTRY, billing_country);
//            values.put(BILLING_STATE, billing_state);
//            values.put(BILLING_SUBURB, billing_suburb);
//            values.put(BILLING_ADD_1, billing_add1);
//            values.put(BILLING_ADD_2, billing_add2);
//            values.put(BILLING_ADD_3, billing_add3);
//            values.put(BILLING_POSTCODE, billing_postcode);
//            values.put(SHIPPING_COUNTRY, shipping_country);
//            values.put(SHIPPING_STATE, shipping_state);
//            values.put(SHIPPING_SUBURB, shipping_suburb);
//            values.put(SHIPPING_ADD_1, shipping_add1);
//            values.put(SHIPPING_ADD_2, shipping_add2);
//            values.put(SHIPPING_ADD_3, shipping_add3);
//            values.put(ORDER_SHIPPING_POSTCODE, shipping_postcode);
//            values.put(ORDER_BY, text_order_by);
//            values.put(ORDER_NOTES, text_order_notes);
//            values.put(DELIVERY_NOTES, text_delivery_notes);
//
//
//            dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_INFO, values, ORDER_ID + " = '" + order_id + "' AND " + PREFIX_ORDER_INFO + " = '" + prefix_name + "'", null);
//
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    //-------------------------updateprodorder------------------//
//    public void updateprodorder(String prefix_name, String order_product_id, String product_id, String qty, String prod_sub_total, String unit_price, String applied_price) {
//        try {
//            dataBaseHelper.createDataBase();
//
//            ContentValues values = new ContentValues();
//            values.put(APL_PRICE, applied_price);
//            values.put(PRODUCT_UNIT_PRICE, unit_price);
//            values.put(PRODUCT_SUB_TOTAL, prod_sub_total);
//            values.put(QTY, qty);
//
//            dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_PRODUCT, values, ORDER_PRODUCT_ID + " = '" + order_product_id + "' AND " + PRODUCT_PRODUCT_ID + " = '" + product_id + "' AND " + PREFIX_ORDER_PRODUCTS + " = '" + prefix_name + "'", null);
//
//            dataBaseHelper.close();
//            values = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//

//    public byte[] ConvertToByteArray(Bitmap b) {
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        b.compress(Bitmap.CompressFormat.PNG, 100, bos);
//        byte[] img = bos.toByteArray();
//
//        return img;
//    }
//
//    public Bitmap ConverByteArrayToBitmap(byte[] img1) {
//
//        Bitmap b1 = BitmapFactory.decodeByteArray(img1, 0, img1.length);
//        return b1;
//    }
}
