package com.apps.olsoncoffee;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.apps.olsoncoffee.Database.DatabaseQueries;
import com.apps.olsoncoffee.model.Cart_items_model;
import com.apps.olsoncoffee.model.JSONParser;
import com.apps.olsoncoffee.model.Methods;
import com.apps.olsoncoffee.model.ModelCartArray;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

public class Billing_details extends AppCompatActivity {

    View view;
    public ArrayList<String> Billing_sArraylist_state = new ArrayList<>();
    public ArrayList<String> Billing_sArraylist_country = new ArrayList<>();
    private int mNotificationsCount = 0;
    LinearLayout proceed_for_payment;
    CheckBox cb;
    public boolean shipped_to_this_address;
    Product_Detail prodDetail;
    DatabaseQueries query;


    private EditText et_billing_email;
    private EditText et_billing_first_name;
    private EditText et_billing_last_name;
    private EditText et_billing_company_name;
    private EditText et_billing_phone_num;
    private EditText et_billing_address_1;
    private EditText et_billing_address_2;
    private EditText et_billing_city;
    private Spinner spinner_billing_country;
    private Spinner spinner_billing_state;
    private EditText et_billing_zip_code;

    ProgressDialog progess;
    ArrayList<ModelCartArray> json_arr = new ArrayList<>();

    String json;

    boolean network;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_details);

        init();
        clickevent();
        setdata();
    }


    private void init() {
        progess = new ProgressDialog(Billing_details.this);
        progess.setIndeterminate(true);
        progess.setMessage("Fetching");
        progess.setCancelable(false);
        Methods.HideKayboard(Billing_details.this);
        network = Methods.isNetworkConnected(getApplicationContext());
        prodDetail = new Product_Detail();
        Toolbar mToolbar = (Toolbar) findViewById(R.id.olson_toolbar);
        setSupportActionBar(mToolbar);

        query = new DatabaseQueries(Billing_details.this);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle(" ");
            mToolbar.setNavigationIcon(R.mipmap.ic_back);

        } catch (Exception e) {
            e.printStackTrace();
        }

        populate_State_spinner(Billing_sArraylist_state);
        populate_Country_spinner(Billing_sArraylist_country);


        proceed_for_payment = (LinearLayout) findViewById(R.id.proceed_for_payment);
        cb = (CheckBox) findViewById(R.id.check_box);

        et_billing_email = (EditText) findViewById(R.id.email_for_billing);
        et_billing_first_name = (EditText) findViewById(R.id.first_name_for_billing);
        et_billing_last_name = (EditText) findViewById(R.id.last_name_for_billing);
        et_billing_company_name = (EditText) findViewById(R.id.company_name_for_biling);
        et_billing_phone_num = (EditText) findViewById(R.id.phone_number_for_billing);
        et_billing_address_1 = (EditText) findViewById(R.id.address_line_1_for_billing);
        et_billing_address_2 = (EditText) findViewById(R.id.address_line_2_for_billing);
        et_billing_city = (EditText) findViewById(R.id.city_name_for_billing);
        spinner_billing_country = (Spinner) findViewById(R.id.billing_detail_choose_country_spinner);
        spinner_billing_state = (Spinner) findViewById(R.id.billing_detail_choose_state_spinner);
        et_billing_zip_code = (EditText) findViewById(R.id.zip_code_for_billing);

    }

    private void setdata() {
        et_billing_email.setText(Utils.billing_email);
        et_billing_first_name.setText(Utils.billing_first_name);
        et_billing_last_name.setText(Utils.billing_last_name);
        et_billing_company_name.setText(Utils.billing_company_name);
        et_billing_phone_num.setText(Utils.billing_phone_num);
        et_billing_address_1.setText(Utils.billing_address_1);
        et_billing_address_2.setText(Utils.billing_address_2);
        et_billing_zip_code.setText(Utils.billing_zip_code);
        et_billing_city.setText(Utils.billing_city);
        spinner_billing_country.setSelection(Utils.billing_country_position);
        spinner_billing_state.setSelection(Utils.billing_state_position);

    }

    private void clickevent() {
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    shipped_to_this_address = true;
                  //  Toast.makeText(getApplicationContext(), "checked", Toast.LENGTH_LONG).show();
                }
                if (cb.isChecked() == false) {
                    shipped_to_this_address = false;
//                    Toast.makeText( getApplicationContext(), "UNchecked", Toast.LENGTH_LONG ).show();

                }
            }
        });

        proceed_for_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Methods.HideKayboard(Billing_details.this);
                getEditTextValues();// calling method

            }
        });
        spinner_billing_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Utils.billing_country_position=arg2;

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        spinner_billing_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Utils.billing_state_position=arg2;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    private void getEditTextValues() {

        Utils.billing_email = et_billing_email.getText().toString();
        Utils.billing_first_name = et_billing_first_name.getText().toString();
        Utils.billing_last_name = et_billing_last_name.getText().toString();
        Utils.billing_company_name = et_billing_company_name.getText().toString();
        Utils.billing_phone_num = et_billing_phone_num.getText().toString();
        Utils.billing_address_1 = et_billing_address_1.getText().toString();
        Utils.billing_address_2 = et_billing_address_2.getText().toString();
        Utils.billing_city = et_billing_city.getText().toString();
        Utils.billing_country = spinner_billing_country.getSelectedItem().toString();
        Utils.billing_state = spinner_billing_state.getSelectedItem().toString();
        Utils.billing_zip_code = et_billing_zip_code.getText().toString();

        Utils.shipping_address_1 = et_billing_address_1.getText().toString();
        Utils.shipping_address_2 = et_billing_address_2.getText().toString();
        Utils.shipping_city = et_billing_city.getText().toString();
        Utils.shipping_country = spinner_billing_country.getSelectedItem().toString();
        Utils.shipping_state = spinner_billing_state.getSelectedItem().toString();
        Utils.shipping_zip_code = et_billing_zip_code.getText().toString();
        if (Utils.billing_email.length()==0 || Utils.billing_first_name.length()== 0 || Utils.billing_last_name.length()== 0 ||
                Utils.billing_phone_num.length()== 0 || Utils.billing_address_1.length()== 0 || Utils.billing_city.length()== 0 || Utils.billing_country.equals( "CHOOSE COUNTRY" ) || Utils.billing_state.equals( "CHOOSE STATE" ) || Utils.billing_zip_code.length()== 0 ) {
            Toast.makeText( getApplicationContext(), "Please enter all the fields.", Toast.LENGTH_LONG ).show();
        } else {

        if (shipped_to_this_address == true) {

            json_arr = query.getJsonproduct();
            json = new Gson().toJson(json_arr);

            System.out.println("gson=" + json);
            if (!network) {
                Methods.conDialog(Billing_details.this);
            } else {
                new FetchCountTask().execute();

            }


        } else {


            Intent intent = new Intent(getApplicationContext(), Shipping_details.class);
            startActivity(intent);
        }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu, menu);

        MenuItem item = menu.findItem(R.id.notification_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        Utils.setBadgeCount(this, icon, MainActivity.fetch_arr.size());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.notification_cart) {

                Intent intent = new Intent(Billing_details.this, Pop_Window.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                //     displayPopupWindow();


        }

        return true;
    }


    private void populate_State_spinner(ArrayList<String> Billing_sArraylist_state) {


//        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        view = inflater.inflate(R.layout.m_spinner,mLinearLayout, false);
//        mLinearLayout.addView(view);

        Billing_sArraylist_state.add("CHOOSE STATE");
        Billing_sArraylist_state.add("Alaska");
        Billing_sArraylist_state.add("Alabama");
        Billing_sArraylist_state.add("Arkansas");
        Billing_sArraylist_state.add("Arizona ");
        Billing_sArraylist_state.add("California");
        Billing_sArraylist_state.add("Colorado");
        Billing_sArraylist_state.add("Connecticut");
        Billing_sArraylist_state.add("District of Columbia");
        Billing_sArraylist_state.add("Delaware");
        Billing_sArraylist_state.add("Florida");
        Billing_sArraylist_state.add("Georgia");
        Billing_sArraylist_state.add("Hawaii");
        Billing_sArraylist_state.add("Iowa");
        Billing_sArraylist_state.add("Idaho");
        Billing_sArraylist_state.add("Illinois");
        Billing_sArraylist_state.add("Indiana");
        Billing_sArraylist_state.add("Kansas");
        Billing_sArraylist_state.add("Kentucky");
        Billing_sArraylist_state.add("Louisiana");
        Billing_sArraylist_state.add("Massachusetts");
        Billing_sArraylist_state.add("Maryland");
        Billing_sArraylist_state.add("Maine");
        Billing_sArraylist_state.add("Michigan");
        Billing_sArraylist_state.add("Minnesota");
        Billing_sArraylist_state.add("Missouri");
        Billing_sArraylist_state.add("Mississippi");
        Billing_sArraylist_state.add("Montana");
        Billing_sArraylist_state.add("North Carolina");
        Billing_sArraylist_state.add("North Dakota");
        Billing_sArraylist_state.add("Nebraska");
        Billing_sArraylist_state.add("New Hampshire");
        Billing_sArraylist_state.add("New Jersey");
        Billing_sArraylist_state.add("New Mexico");
        Billing_sArraylist_state.add("New York");
        Billing_sArraylist_state.add("Ohio");
        Billing_sArraylist_state.add("Oklahoma");
        Billing_sArraylist_state.add("Pennsylvania");
        Billing_sArraylist_state.add("Rhode Island");
        Billing_sArraylist_state.add("South Carolina");
        Billing_sArraylist_state.add("South Dakota");
        Billing_sArraylist_state.add("Tennessee");
        Billing_sArraylist_state.add("Texas");
        Billing_sArraylist_state.add("Utah");
        Billing_sArraylist_state.add("Virginia");
        Billing_sArraylist_state.add("Vermont");
        Billing_sArraylist_state.add("Washington");
        Billing_sArraylist_state.add("Wisconsin");
        Billing_sArraylist_state.add("West Virginia");
        Billing_sArraylist_state.add("Wyoming");

        Spinner drop_down = (Spinner) findViewById(R.id.billing_detail_choose_state_spinner);

        drop_down.setAdapter(new ArrayAdapter<String>(Billing_details.this, android.R.layout.simple_dropdown_item_1line, Billing_sArraylist_state));

    }

    private void populate_Country_spinner(ArrayList<String> Billing_sArraylist_country) {


//        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        view = inflater.inflate(R.layout.m_spinner,mLinearLayout, false);
//        mLinearLayout.addView(view);

        Billing_sArraylist_country.add("CHOOSE COUNTRY");
        Billing_sArraylist_country.add("United States");

        Spinner drop_down = (Spinner) findViewById(R.id.billing_detail_choose_country_spinner);

        drop_down.setAdapter(new ArrayAdapter<String>(Billing_details.this, android.R.layout.simple_dropdown_item_1line, Billing_sArraylist_country));

    }


    /*
 Updates the count of notifications in the ActionBar.
  */
    private void updateNotificationsBadge(int count) {
//        mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }


    /*
  Sample AsyncTask to fetch the notifications count
  */
    class FetchCountTask extends AsyncTask<Void, Void, Integer> {
        String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progess.show();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            response = billing_details();


            return null;
        }

        @Override
        public void onPostExecute(Integer count) {
//            updateNotificationsBadge(count);

            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.optString("status").equals("true")) {
                    String data = jobj.optString("Data");
                    SharedPreferences prefs = getSharedPreferences( "Olson", MODE_PRIVATE );
                    String pwd=prefs.getString("pwd","");
                    if (prefs.getString("pwd","").length()==0) {
                        progess.dismiss();
                        Intent intent = new Intent(getApplicationContext(), Pay_Pal_Activity.class);
                        intent.putExtra("shipping_cost", data);
                        intent.putExtra("json", json);
                        startActivity(intent);
                        finish();
                    }else {
                        new paymentconfermation(data).execute();
                    }
                } else {
                    progess.dismiss();
                    Methods.dialog(Billing_details.this, "Alert!", "Network Response Error.", android.R.drawable.ic_dialog_info);

                }
            } catch (Exception e) {
                e.printStackTrace();
                progess.dismiss();

                Methods.dialog(Billing_details.this, "Alert!", "Network Response Error.", android.R.drawable.ic_dialog_info);

            }

        }
    }
    class paymentconfermation extends AsyncTask<String, Void, String> {

        String response;
        String shipping_cost, json;
        double purchased_cost;
        double grand_total;

        public paymentconfermation(String data) {
            this.shipping_cost=data;
            purchased_cost = Pop_Window.purchased_total;
            grand_total = Double.parseDouble(shipping_cost) + purchased_cost;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            response = getdata(shipping_cost,purchased_cost,grand_total);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progess.dismiss();
            //    { "status":"true","order_id":23}
            try {
                JSONObject jobj = new JSONObject(response);
                query.deletecart();
                Intent intent = new Intent(Billing_details.this, Congratulations.class);
                startActivity(intent);
                finish();
            } catch (Exception e) {
            }
        }
    }

    private String getdata(String shipping_cost, double purchased_cost, double grand_total) {
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("first_name", Utils.billing_first_name)
                .appendQueryParameter("last_name", Utils.billing_last_name)
                .appendQueryParameter("address_line_one", Utils.billing_address_1)
                .appendQueryParameter("address_line_two", Utils.billing_address_2)
                .appendQueryParameter("city", Utils.billing_city)
                .appendQueryParameter("state", Utils.billing_state)
                .appendQueryParameter("country", Utils.billing_country)
                .appendQueryParameter("post_code", Utils.billing_zip_code)
                .appendQueryParameter("delivery_address_line_one", Utils.shipping_address_1)
                .appendQueryParameter("delivery_address_line_two", Utils.shipping_address_2)
                .appendQueryParameter("delivery_order_city", Utils.shipping_city)
                .appendQueryParameter("delivery_order_state", Utils.shipping_state)
                .appendQueryParameter("delivery_order_country", Utils.shipping_country)
                .appendQueryParameter("delivery_order_post_code", Utils.shipping_zip_code)
                .appendQueryParameter("sub_total", String.valueOf(purchased_cost))
                .appendQueryParameter("shipping", shipping_cost)
                .appendQueryParameter("grand_total", String.valueOf(grand_total))
                .appendQueryParameter("product_details", json);

        String url = Utils.get_order;
        String response = parser.getJSONFromUrl(url, builder); // builder will be pases blank

        System.out.println("Response==" + response);
        return response;
    }
    @Override
    protected void onResume() {
        super.onResume();
        MainActivity.fetch_arr = query.getcatproduct();
        updateNotificationsBadge(MainActivity.fetch_arr.size());
    }

    public String billing_details() {
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("product_details", json)
                .appendQueryParameter("postcode", Utils.billing_zip_code);

        String url = Utils.get_billing_details;
        Log.i("URL", url);

        String response = parser.getJSONFromUrl(url, builder); // builder will be pases blank
        Log.i("Response", "Get Billing Detail" + response);
        return response;
    }


}