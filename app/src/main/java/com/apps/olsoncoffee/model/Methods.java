package com.apps.olsoncoffee.model;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.apps.olsoncoffee.R;

/**
 * Created by user on 5/27/2016.
 */
public class Methods {
    // **************************************Connection check**************************************
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    // **************************************Connection Dialog**************************************
    public static void conDialog(final Activity c) {
        AlertDialog.Builder alert = new AlertDialog.Builder(c, R.style.AppTheme_Dialog);

        alert.setTitle("Internet connection unavailable.");
        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
        alert.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                        c.startActivity(new Intent(
                                Settings.ACTION_WIRELESS_SETTINGS));
                    }
                });

        alert.show();
    }

    //*************************Hide KeyBoard**************************//
    public static void HideKayboard(Activity c) {
        // Check if no view has focus:
        View view = c.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**********************************************
     * Simple dialog
     ********************************************/
    public static void dialog(Activity a,String title, String msg, int icon) {
        new AlertDialog.Builder(a, R.style.AppTheme_Dialog)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })


                .setIcon(icon)
                .show();

    }
}
