package com.apps.olsoncoffee.model;

/**
 * Created by user on 13/5/16.
 */
public class Cart_items_model {
    public String product_icon;
    public String product_name;
    public String product_types;
    public String product_quantity;
    public String product_id;
    public double total_price;
    public double subtotal;
    public int id;
    public int no_of_products;
}
