package com.apps.olsoncoffee.model;

import android.net.Uri;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class JSONParser {
    // constructor
    public JSONParser() {

    }

    /*             key value pairs
                builder = new Uri.Builder()
                        .appendQueryParameter("auth_code", auth_code);*/
    public String getJSONFromUrl(String urlAddress, Uri.Builder builder) {
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;

        try {
            URL url = new URL(urlAddress);
            HttpURLConnection con = (HttpURLConnection) url
                    .openConnection();

            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            con.setRequestProperty("Content-Type", "application/json");
            con.setReadTimeout(15000);
            con.setConnectTimeout(15000);
            con.setRequestMethod("POST");
            con.setDoInput(true);
            con.setDoOutput(true);

            String query = builder.build().getEncodedQuery();

            //adds key value pair to your request
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            // use this response code to get status codes like 200 is for
            // success,500 for error on server side etc
            int responseCode = con.getResponseCode();
            System.out.println(" code hh : " + responseCode);

            in = con.getInputStream();
            // in = url.openStream(); remove this line
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    in, "iso-8859-1"), 8);
            String line;
            while ((line = reader.readLine()) != null) {
                tempData.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return tempData.toString();
    }
}