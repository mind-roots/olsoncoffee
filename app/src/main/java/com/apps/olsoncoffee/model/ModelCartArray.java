package com.apps.olsoncoffee.model;

/**
 * Created by user on 5/27/2016.
 */
public class ModelCartArray {
    public String quantity;
    public String varietyNames;
    public String product_id;
    public String id;
    public String names;
    public String images;
    public String prices;
    public String subtotal;
}
