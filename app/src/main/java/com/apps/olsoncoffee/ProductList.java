package com.apps.olsoncoffee;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Toast;

import com.apps.olsoncoffee.Database.DatabaseQueries;
import com.apps.olsoncoffee.adapter.products_adapter;
import com.apps.olsoncoffee.model.JSONParser;
import com.apps.olsoncoffee.model.Methods;
import com.apps.olsoncoffee.model.product_model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductList extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private LinearLayoutManager layoutManager;
    Activity context;
    private products_adapter mAdapter;
    List<product_model> mData = new ArrayList<>();
    private ProgressDialog progress;
    public static Menu newmenu;
    DatabaseQueries query;
    boolean network;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        init();
        Intent intent = getIntent();
        String category_type = intent.getStringExtra("category_type");
        mData.clear();
        if (!network) {
            Methods.conDialog(ProductList.this);
        } else {
            new get_product_list(category_type).execute();
        }


        clickEvents();

    }

    private void clickEvents() {


    }


    /**************************************
     * Initialize UI elements
     ************************************/

    private void init() {
        network = Methods.isNetworkConnected(getApplicationContext());
        Toolbar mToolbar = (Toolbar) findViewById(R.id.olson_toolbar);
        setSupportActionBar(mToolbar);

        query = new DatabaseQueries(ProductList.this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle(" ");
            mToolbar.setNavigationIcon(R.mipmap.ic_back);

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Initialize recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.cart_menu, newmenu);

        MenuItem item = newmenu.findItem(R.id.notification_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        Utils.setBadgeCount(this, icon, MainActivity.fetch_arr.size());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.notification_cart) {
                Intent intent = new Intent(ProductList.this, Pop_Window.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                //     displayPopupWindow();

        }

        return true;
    }

    class get_product_list extends AsyncTask<Void, Void, Void> {
        String type;

        public get_product_list(String category_type) {
            this.type = category_type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(ProductList.this);
            progress.setIndeterminate(true);
            progress.setMessage("Fetching");
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String response = products(type); //getting response


            try {
                JSONObject jobj = new JSONObject(response);

                JSONArray data_array = jobj.getJSONArray("Data");

                for (int i = 0; i < data_array.length(); i++) {
                    JSONObject obj = data_array.getJSONObject(i);

                    product_model productModel = new product_model();
                    productModel.product_id = obj.getString("product_id");
                    productModel.product_name = obj.getString("name");
                    productModel.product_image = obj.getString("image");

                    mData.add(productModel);

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();
            mAdapter = new products_adapter(ProductList.this, mData);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    public String products(String type) {
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("", "");

        String url = Utils.get_coffee + "&category_name=" + type;

        String response = parser.getJSONFromUrl(url, builder); // builder will be pases blank
        Log.i("Response", "Get Products" + response);
        return response;
    }

    public void updateNotificationsBadge(int count) {
        //   mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainActivity.fetch_arr = query.getcatproduct();
        updateNotificationsBadge(MainActivity.fetch_arr.size());
    }


}
