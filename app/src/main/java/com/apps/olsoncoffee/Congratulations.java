package com.apps.olsoncoffee;

import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Congratulations extends AppCompatActivity {
    LinearLayout go_back_to_home;
    public static Menu newmenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_congratulations );

        go_back_to_home = (LinearLayout) findViewById( R.id.go_back_to_home );

        go_back_to_home.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              finish();
            }
        } );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.cart_menu, newmenu);

        MenuItem item = newmenu.findItem(R.id.notification_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        Utils.setBadgeCount(this, icon, MainActivity.fetch_arr.size());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.notification_cart) {
            if (MainActivity.fetch_arr.size() != 0) {
                Intent intent = new Intent(Congratulations.this, Pop_Window.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                //     displayPopupWindow();
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), "No Items in Cart", Toast.LENGTH_LONG);
                toast.setGravity( Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 144);
                toast.show();
            }

        }

        return true;
    }
}
