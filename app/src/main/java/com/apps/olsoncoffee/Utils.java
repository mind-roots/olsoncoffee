package com.apps.olsoncoffee;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

import java.net.URL;

/**
 * Created by user on 6/5/16.
 */
public class Utils {

   // public static String base_url = "http://olsonhousecoffeehosting.com/webservice.php?";
   public static String base_url = "http://cloudart.com.au/projects/olson/webservice.php?";
    public static String get_coffee = base_url + "service_type=get_coffee";
    public static String get_product_details = base_url + "service_type=get_product";
    public static String get_front_image = base_url + "service_type=get_month_special";
    public static String get_billing_details = base_url + "service_type=shipping";
    public static String get_order = base_url + "service_type=get_order";
    public static String get_months_special = base_url + "service_type=get_sku_info";
    public static BadgeDrawable badge;
    public static String shipping_email;
    public static String shipping_first_name;
    public static String shipping_last_name;
    public static String shipping_company_name;
    public static String shipping_phone_num;
    public static String shipping_address_1;
    public static String shipping_address_2;
    public static String shipping_city;
    public static String shipping_country;
    public static String shipping_state;
    public static String shipping_zip_code;
    public static int shipping_country_position=0;
    public static int shipping_state_position=0;

    public static String billing_email;
    public static String billing_first_name;
    public static String billing_last_name;
    public static String billing_company_name;
    public static String billing_phone_num;
    public static String billing_address_1;
    public static String billing_address_2;
    public static String billing_city;
    public static String billing_country;
    public static String billing_state;
    public static String billing_zip_code;
    public static int billing_country_position=0;
    public static int billing_state_position=0;

    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {
        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId( R.id.ic_badge );
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable( context );
        }

        badge.setCount( count );
        icon.mutate();
        icon.setDrawableByLayerId( R.id.ic_badge, badge );
    }

}